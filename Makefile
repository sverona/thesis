tex: clean
	for orgfile in chapters/*.org; do \
		pandoc -f org -t latex --top-level-division=chapter $$orgfile > $${orgfile%.*}.tex; \
        done
	cat chapters/*.tex > thesis.tex

thesis: tex
	xelatex thesis

thesis-with-references: tex
	xelatex thesis
	bibtex thesis
	xelatex thesis
	xelatex thesis

clean:
	rm -f thesis.{aux,log,out,tex}


.PHONY: tex thesis thesis-with-references clean
