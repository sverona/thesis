\hypertarget{preliminaries}{%
\chapter{Preliminaries}\label{preliminaries}}

This chapter introduces the mathematics necessary to read Chapter 3. The
main results draw from combinatorial lattice theory and the
representation theory of Lie algebras.

\hypertarget{lattice-theory}{%
\section{Some aspects of combinatorial lattice
theory}\label{lattice-theory}}

In this section we present a number of standard definitions and notions
concerning posets and lattices, in particular the \emph{diamond-colored
distributive lattices} (DCDLs). DCDLs provide the setting for the
graph-theoretic arguments and algorithms appearing in the sequel.

The primary reference for this section is \cite{donnelly-poset-models}.

\hypertarget{partially-ordered-sets}{%
\subsection{Partially ordered sets}\label{partially-ordered-sets}}

\begin{definition}
Let $P$ be a set. A \textit{partial order} is a binary relation $\leq$ on $P$ satisfying the following axioms for all $a, b, c \in P$.
\begin{enumerate}
\tightlist
\item $\leq$ is \textit{reflexive}: $a \leq a$;
\item $\leq$ is \textit{transitive}: $a \leq b$, $b \leq c \implies a \leq c$; and
\item $\leq$ is \textit{antisymmetric}: $a \leq b, b \leq a \implies a = b$.
\end{enumerate}

A set $P$ equipped with a partial order $\leq$ is called a \textit{partially ordered set}, or just \textit{poset}.
\end{definition}

\begin{definition}
Let $P$ be a poset. If $a, b \in P$, and for any $x \in P$, \(a \leq x, x \leq b \implies x \in \{a, b\},\) we say $b$ \textit{covers} $a$ and write $a \to b$.
\end{definition}

\begin{definition}
To any poset $L$ we may associate a directed graph $H$, called a \textit{Hasse
diagram}, whose vertex set is $L$ and whose edge set is given by the
covering relation; that is, there is an edge $a \to b$ in $H$ only if $a \to b$ in $L$.
\end{definition}

In the sequel we shall speak of lattices and their Hasse diagrams
interchangeably.

\begin{definition}
Given two posets $P$, $Q$, with respective orders $\leq_P$, $\leq_Q$, we
may define their \textit{Cartesian product} $P \boxtimes Q$ as a poset in its own
right by equipping the set $P \boxtimes Q = \{(p, q): p \in P, q \in Q\}$
with the \textit{product order} $\leq_{P \boxtimes Q}$, which is defined so that
$(p_1, q_1) \leq_{P \boxtimes Q} (p_2, q_2)$ iff $p_1 \leq_P p_2$ and
$q_1 \leq_Q q_2$.
\end{definition}

Note that \((p_1, q_1) \to (p_2, q_2)\) in \(P \boxtimes Q\) iff
\(p_1 \to p_2\) in \(P\) and \(q_1 = q_2\) or vice versa; that is, the
Hasse diagram of the product is the Cartesian product of the Hasse
diagrams of its components.

\begin{definition}
An element of a poset $m \in P$ is \textit{maximal} if $m \leq x \implies x = m$, and \textit{maximum} if $x \leq m$ for all $x \in P$. The dual notions (\textit{minimal}, \textit{minimum}) are apparent.
We represent the maximum (minimum) element of the poset $P$, if it exists, by $\top_P$ ($\bot_P$.)
\end{definition}

\begin{definition}
A finite poset is said to be \textit{ranked} if there exists a function $\rho : P \to \mathbb{N} \cup \{0\}$ such that
\begin{enumerate}
\item for any $a, b \in L$, $\rho(b) - \rho(a) = 1$ whenever $a \to b$;
\item if $x \in P$ is minimal, $\rho(x) = 0$.
\end{enumerate}
For any natural number $n$, the $n$th \textit{rank} of a ranked poset $(P, \rho)$ is the preimage $\rho^{-1}[n]$.
\end{definition}

\begin{definition}
The \textit{height} of a finite ranked poset is $h(L) = \sup_{x \in L} \rho(x)$.
\end{definition}

\begin{definition}
The \textit{rank-generating function} of a ranked poset is the formal power series $$RGF(L; q) = \sum_{x \in L} q^{\rho(x)} = \sum_{n=0}^\infty \left\vert \rho^{-1}[n]\right\vert q^n.$$
\end{definition}

\begin{definition}
Given a poset $P$, a \textit{weight function} $\mathbf{\omega} : P \to \mathbb{Z}^n$ assigns to each poset element $x$ an $n$-tuple of integer \textit{weights} $\mathbf{\omega}(x) = \begin{bmatrix} \omega_1(x), & \cdots, & \omega_n(x) \end{bmatrix}$.
\end{definition}

\begin{definition}
For a given poset $P$ and weight function $\mathbf\omega : P \to \mathbb Z^n$, the \textit{weight-generating function} of a poset is the formal Laurent series $$WGF(L; \mathbf{z}) = \sum_{x \in L} \mathbf{z}^{\mathbf{\omega}(x)} = \sum_{x \in L} \prod_{i = 1}^n z_i^{\omega_i(x)}.$$ \end{definition}

\begin{definition}
An \textit{order ideal} (sometimes \textit{down-set} or \textit{lower set}) of a poset $P$ is a set $I \subseteq P$ such that for any $x \in P$, $y \in I$, if $x \leq y$ then $x \in I$.
\end{definition}

In finite posets, order ideals can be uniquely identified with their
maximal elements; if \(I \subseteq P\) is an order ideal,
\(M \subseteq I\) its set of maximal elements, then
\(I = \bigcup_{m \in M} \{x \in P : x \leq m\}.\)

\hypertarget{lattices}{%
\subsection{Lattices}\label{lattices}}

\begin{definition}
A \textit{lattice} $L$ is a partially ordered set such that for any $a, b \in L$,
\begin{itemize}
\tightlist
\item the set $\{x \in L: x \leq a, x\leq b \}$ has a maximum element $a \wedge b$, and
\item the set $\{x \in L: a \leq x, b\leq x \}$ has a minimum element $a \vee b$.
\end{itemize}

The symbols $\vee$, $\wedge$ are read as ``meet'' and ``join'' respectively.
\end{definition}

\begin{definition}
A lattice $L$ is \textit{distributive} if for any $a, b, c \in L$, we have \((a \wedge b) \vee c = (a \vee c) \wedge (b \vee c),\) or equivalently, \((a \vee b) \wedge c = (a \wedge c) \vee (b \wedge c);\) that is, if meet distributes over join, or vice versa.
\end{definition}

\hypertarget{lattices-from-posets}{%
\subsection{Lattices from posets}\label{lattices-from-posets}}

\begin{definition}
Let $P$ be a poset. The poset $J(P)$ is the set of all order ideals of $P$ ordered by containment.
\end{definition}

\begin{proposition}
For any poset $P$, $J(P)$ is a distributive lattice.
\end{proposition}

\begin{proof}
To show $J(P)$ is a lattice, let $I, J \subseteq P$  be order ideals of $P$. We claim $I \vee J = I \cup J$ and $I \wedge J = I \cap J$.

First we show that $I \cup J$, $I \cap J$ are actually order ideals. Suppose $x \in I \cup J$ and $y \leq x$. Without loss of generality assume $x \in I$. Then since $I$ is an order ideal, $y \in I \subset I \cup J$.

Similarly, suppose $x \in I \cap J$ and $y \leq x$. Then $x \in I$ and $x \in J$, so $y \in I$, $y \in J$. Thus $y \in I \cap J$.

Now we must show $I \cup J = I \vee J$. Suppose $K$ is an order ideal of $P$ such that $I \subset K$, $J \subset K$. Then clearly $I \cup J \subset K$. Thus $I \cup J$ is the minimum element of the set $\{K \in J(P) : K \leq I, K \leq J\}$.

Similarly, if $K$ is an order ideal of $P$ such that $K \subset I$, $K \subset J$, then $K \subset I \cap J$. Thus $I \cap J$ is the maximum element of the set $\{K \in J(P) : I \leq K, J \leq K\}$.

Therefore $J(P)$ is a lattice. Showing that $J(P)$ is distributive amounts to proving that intersections distribute over unions and vice versa, which is well-known.
\end{proof}

\hypertarget{birkhoffs-representation-theorem}{%
\subsection{Birkhoff's representation
theorem}\label{birkhoffs-representation-theorem}}

In fact, any finite distributive lattice is representable as \(J(P)\)
for some poset \(P\).

\begin{definition}
An element $x \in L$ is said to be \textit{join-irreducible} if $x \not = \min L$ and $x$ cannot be written as $u \vee v$, where $u,v \not = x$.
\end{definition}

\begin{proposition}
In a finite lattice $L$, an element $x$ is join-irreducible iff it covers exactly one element.
\end{proposition}

\begin{proof}
\textit{If}: By contraposition; if $x$ covers distinct elements $u, v \in L$, then $x = u \vee v$, so $x$ is not join-irreducible. If $x$ covers zero elements of $L$, then $x = \min L$ and so is not join-irreducible.

\textit{Only if}: Suppose $x$ covers exactly one element $y \in L$. Then clearly $x \not= \min L$. Now suppose contrariwise that $x = u \vee v$, where $u, v \not = x$. Since $u, v \leq x$, we must have $u, v \leq y$. But then $u \vee v = y$, which is a contradiction.
\end{proof}

\begin{definition}
We denote the subposet of all join-irreducible elements of $L$, with order induced by $L$, by $j(L)$.
\end{definition}

The following theorem is called the Fundamental Theorem for Finite
Distributive Lattices (or \emph{FTFDL}) by \cite{stanley-ec2}.

\begin{theorem}[Birkhoff]
For any finite poset $P$, $j(J(P)) \approx P$. For any finite distributive lattice $L$, $J(j(L))$ is order isomorphic to $L$.
\end{theorem}

The ``function''\footnotemark{} \(J\) maps order ideals of \(P\) to
elements in \(L\); the ``function'' \(j\) goes the other way. Thus, for
\(I \subseteq P\), \(x \in L\), we can consider \(J(I) \in L\) and
\(j(x) \subseteq P\).
\footnotetext{We stop short of defining these as functions for technical issues; strictly speaking, the ``set'' of all posets is actually a proper class.}

The preceding theorem assures that every finite distributive lattice is
ranked, viz.~\(\rho(x) = \vert j(x) \vert.\) It follows that for any two
elements \(x, y \in L\) with \(x \leq y\),
\(\rho(y) - \rho(x) = \vert j(y) \setminus j(x) \vert.\)

\hypertarget{diamond-colorings}{%
\subsection{Diamond-colorings}\label{diamond-colorings}}

\begin{definition}
A $k$-\textit{coloring} of a poset $P$ is a function $c : P \to (\mathbb{N} \cap [1, k])$. We say $P$ is \textit{colored by} $c$.
\end{definition}

\begin{definition}
Let a poset $P$ be colored by some function $c$, and consider the Hasse diagram $H$ of $J(P)$ as a directed graph. This graph has an edge from order ideal $I$ to order ideal $J$ iff $J = I \cup \{v\}$ for some $v \in P$. Thus we may naturally color the
edge $I \to J$ by $c(v)$. When we regard the edges of $J(P)$ as being
colored (by $c$) in this way, we shall call it a \textit{diamond-colored distributive lattice}, or just a \textit{DCDL.}
\end{definition}

\begin{definition}
A \textit{diamond} of a distributive lattice is the sublattice induced by four elements $w, x, y, z$ such that $w \to x \to z$, $w \to y \to z$. In the Hasse diagram, we say the edge $w \to x$ is \textit{parallel to} $y \to z$. Similarly, $w \to y$ is parallel to $x \to z.$ (See Figure 2.1.)
\end{definition}

\begin{figure}
\centering
\begin{pspicture}(-2,-2)(2,2)
\psdot*(0,-1.5)
\uput*[270](0,-1.5){$w$}
\psdot*(-1.5, 0)
\uput*[180](-1.5,0){$x$}
\psdot*(1.5,0)
\uput*[0](1.5,0){$y$}
\psdot*(0,1.5)
\uput*[90](0,1.5){$z$}
\psline[linewidth=1pt,linecolor=black,arrows=->](0,-1.5)(-1.5,0)
\psline[linewidth=1pt,linecolor=black,arrows=->](0,-1.5)(1.5,0)
\psline[linewidth=1pt,linecolor=black,arrows=->](-1.5,0)(0,1.5)
\psline[linewidth=1pt,linecolor=black,arrows=->](1.5,0)(0,1.5)
\end{pspicture}
\caption{A typical diamond.}
\end{figure}

We may regard parallelism as being naturally transitive; then the
parallel edges of any DCDL fall into ``monochromatic'' equivalence
classes.

\begin{proposition}
In a DCDL, parallel edges have the same color.
\end{proposition}

\begin{proof}
Let $L$ be a DCDL colored by $c$, and let $w, x, y, z$ be the vertices of a diamond, identified as in the above definition. Consider the order ideals $j(w), j(z)$. The set $j(z) \setminus j(w) \subset P$ has two elements $u, v$.  Without loss of generality, write \(j(x) = j(w) \cup \{ u \}\) and \(j(y) = j(w) \cup \{ v \}\). Then \(j(z) = j(x) \cup \{v\} = j(y) \cup \{u\}.\) Thus the edges $w\to x$ and $y \to z$ have color $c(u)$ while the other two edges have color $c(v)$.
\end{proof}

\hypertarget{monochromatic-components}{%
\subsection{Monochromatic components}\label{monochromatic-components}}

\begin{definition}
Let $L$ be a DCDL whose edges are colored by $[n]$, and let $k \in [n].$ A $k$-\textit{monochromatic component} of $L$ or just $k$-\textit{component} is a subset $C \subset L$ such that
\begin{enumerate}
\tightlist
\item $C$ is a subposet of $L$ under the induced order;
\item if $x \to y$ in $C$, then $x \to y$ in $L$;
\item and $C$ is maximal with respect to the property that all edges have color $k$.
\end{enumerate}
\end{definition}

In this subsection, we prove the (nontrivial) fact that any
\(k\)-component of a DCDL has a distributive lattice structure. The
following definition, which describes the ``preimage'' under \(J\) of a
\(k\)-component, will be necessary.

\begin{definition}
Let $P$ be a poset with vertices colored by $[n]$, and let $k \in [n].$ A \textit{$k$-subordinate} of $P$ is a subset $S \subseteq P$ such that
\begin{enumerate}
\tightlist
\item $S$ is a subposet of $P$ under the induced order;
\item all elements of $S$ have color $k$;
\item and there exist order ideals $I, J \subset P$ where no maximal element of $I$ has color $k$ and no minimal element of $P \setminus J$ has color $k$ and $S = J \subset I$.
\end{enumerate}
We say that $I$ and $J$ are \textit{bounding ideals} for $S$; $I$ is \textit{lower bounding} and $J$ is \textit{upper bounding.}
\end{definition}

The next proposition establishes a one-to-one correspondence between
\(k\)-subordinates of a poset \(P\) and \(k\)-components of its
corresponding lattice \(L = J(P)\).

\begin{proposition}
Let $L = J(P)$ be a DCDL. Let $k$ be an edge or vertex color.
\begin{enumerate}
\item Let $C$ be a $k$-component of $L$. Then $C$ is a (distributive) sublattice of $L$, $S = \top_C \setminus \bot_C$ is a $k$-subordinate of $P$ with bounding ideals $\top_C$ and $\bot_C$, and $C \approx J(S).$
\item Let $S$ be a $k$-subordinate of $P$ with lower bounding ideal $I$ and upper bounding ideal $J$. Then $I$ and $J$ are the minimum and maximum elements of some $k$-component $C \subset L$, and $j(C) \approx S.$
\end{enumerate}
\end{proposition}

\begin{proof}
See \cite{donnelly-poset-models}, Proposition 13 and Theorem 14.
\end{proof}

\begin{proposition}
Let $C$ be a $k$-component of $L$. Then $C$ can be written as a Cartesian product of chains if and only if its corresponding $k$-subordinate $S \subseteq j(L)$ can be written as a disjoint union of chains.
\end{proposition}

\begin{proof}
\textit{If:} Suppose $S \subseteq P = j(L)$ can be written as a disjoint union of chains: $S = \bigsqcup_{i=1}^n C_i.$ We make the following claims.
\begin{enumerate}
\tightlist
\item $C \approx J(S) = J(\bigsqcup_{i=1}^n C_i).$ This follows from the FTFDL and the preceding proposition.
\item $J(\bigsqcup_{i=1}^n C_i) = \bigboxtimes_{i=1}^n J(C_i)$. To see this, note that every order ideal of $\bigsqcup_{i=1}^n C_i$ corresponds uniquely to a choice of at most one element from each $C_i.$
\item Each $J(C_i)$ is a chain with $|J(C_i)| = |C_i| + 1.$
\end{enumerate}

\textit{Only if:} Since the correspondences between order ideals of $S$ and choices of elements from each $C_i$ and between $S$ and $C$ are both one-to-one, this proof reverses.
\end{proof}

\hypertarget{incremental-lattices}{%
\subsection{Incremental lattices}\label{incremental-lattices}}

This section characterizes distributivity in a way more genial to our
needs.

\begin{definition}
The \textit{$n$-dimensional integer lattice} $\mathbb Z^n$ is the set of all integer $n$-tuples ordered elementwise.
\end{definition}

If \(\mathbf x, \mathbf y \in \mathbb Z^n\), then
\(\mathbf x \vee \mathbf y\) (resp. \(\mathbf x \wedge \mathbf y\)) is
given by the elementwise maximum (resp. minimum) of \(\mathbf x\) and
\(\mathbf y\).

\begin{lemma}
Any sublattice of $\mathbb Z^n$ is distributive.
\end{lemma}

\begin{proof}
It suffices to show that, for $x, y, z \in \mathbb Z$, we have \(\min\{\max\{x, y\}, \max \{x, z\}\} = \max\{x, \min\{y, z\}\}.\)
and by symmetry we need only check the cases $x \leq y \leq z,$ $y \leq x \leq z,$ $y \leq z \leq x.$ This verification is left to the reader.
\end{proof}

\begin{definition}
A lattice $L$ is \textit{incremental} if there is an injective homomorphism $\phi: L \to \mathbb Z^n$ for some $n$. We say that $L$ \textit{embeds into \(\mathbb Z^n \)} and call the function $\phi$ an \textit{embedding.}
\end{definition}

\begin{proposition}
A finite lattice is incremental if and only if it is distributive.
\end{proposition}

\begin{proof}
\textit{If.} Suppose $L$ is a finite distributive lattice. By Birkhoff's theorem, $L$ is the lattice of order ideals of some poset $P = j(L)$. Write the vertex set of $P$ as a union of disjoint chains, say $P = \bigcup_{i=1}^n C_i = \bigcup_{i=1}^n \{c_{i, k}\}_k,$ where $c_{i, 1} \to c_{i, 2}\to \dots$

Now define $\phi : L \to \mathbb Z^n$ as follows: for each $x \in L$ and $i = 1, \dots, n$, the $i$th component of $\phi(x)$ is $\phi(x)_i = |C_i \cap j(x)|.$ It is apparent that this value is the maximum $k = 1, 2, \dots$ for which $c_{i,k} \in j(x)$, or $0$ if $C_n$ and $j(x)$ are disjoint.

Since $P = j(L)$ is finite, $\phi$ is well-defined. We make the following claims.
\begin{enumerate}
\tightlist
\item $\phi$ is injective. Suppose $\phi(x) = \phi(y)$; then the maximal elements of $j(x)$ and $j(y)$ are the same, so $x = y$.
\item $\phi$ is a homomorphism. Let $x, y \in L.$ Then for any $i$,
\begin{align*}
\phi(x \vee y)_i &= |C_i \cap j(x \vee y)| \\
&= |C_i \cap (j(x) \cup j(y))| \\
&= |(C_i \cap j(x)) \cup (C_i \cap j(y))| \\
&= \max\{|C_i \cap j(x)|, |C_i \cap j(y)|\} \\
&= \max\{\phi(x)_i, \phi(y)_i\}.
\end{align*}
An analogous argument shows that $\phi(x \wedge y)_i = \min\{\phi(x)_i, \phi(y)_i\}.$
\end{enumerate}
\textit{Only if.} Suppose $L$ is a finite incremental lattice. Let $\phi : L \to \mathbb Z^n$ be an injective homomorphism. Suppose contrariwise that $L$ is not distributive. Then there exist $x, y, z \in L$ so that $(x \vee y) \wedge (x \vee z) \not= x \vee (y \wedge z).$ Since $\phi$ is injective, we must have $\phi((x \vee y) \wedge (x \vee z)) \not= \phi(x \vee (y \wedge z)).$ Since $\phi$ is a homomorphism, we have $(\phi(x) \vee \phi(y)) \wedge (\phi(x) \vee \phi(z)) \not= \phi(x) \vee (\phi(y) \wedge \phi(z)).$ But $\phi(L)$ is a sublattice of $\mathbb Z^n$, and so is distributive; so this is a contradiction.
\end{proof}

Therefore it suffices to study the distributive lattices in Chapter 3 as
sublattices of \(\mathbb Z^n.\) Doing so gives us a nice
characterization of their join-irreducible elements.

\begin{proposition}
Let $L$ be a finite distributive lattice, and let $\phi : L \to \mathbb Z^n$ be an embedding. Write $\phi$ as a vector $[\phi_1, \phi_2, \dots, \phi_n],$ where $\phi_k : L \to \mathbb Z.$ Say $x \in L$ is \textit{$k$-decrementable} if there is another element $y \in L$ such that $\phi_i(x) = \phi_i(y)$ if $i \not = k$ and $\phi_k(x) = \phi_k(y) + 1.$ Then $x \in L$ is join-irreducible if and only if it is $k$-decrementable for exactly one $k$.
\end{proposition}

\begin{proof}
$x \in L$ is $k$-decrementable for exactly one $k$ if and only if it covers exactly one element.
\end{proof}

\begin{corollary}
With the above setup, let $\phi_k(L) = \{m_k, m_k + 1, ..., M_k\}$ be the image of $\phi_k$ in $\mathbb Z.$ For each $l \in \phi_k(L)$, let $L_{k, l}$ be the sublattice of $l$ formed by those elements that have $k$th coordinate $l$. Let $\bot_{k, l} = \min L_{k, l}$. Then $$j(L) = \bigcup_{k=1}^n \bigcup_{l = m_k + 1}^{M_k} \{\bot_{k, l}\}.$$
\end{corollary}

\begin{proof}
First, we claim that any element $\bot_{k, l}$ covers at most one element. Suppose contrariwise that $\bot_{k, l}$ covers two elements; then it is decrementable in two positions $k$, $k'$. But since $L$ is distributive, decrementing $k'$ gives an element $\bot'_{k, l} \leq \bot_{k, l}$, and by construction $\bot'_{k, l} \in L_{k, l},$ which contradicts the minimality of $\bot_{k, l}.$

Obviously $x_{k, m_k} = \bot,$ which is not join-irreducible. Thus the right-hand set is included in the left-hand set.

By the proposition, any join-irreducible element $y \in L$ is $k$-decrementable for exactly one $k$. We then have $y = \bot_{k, \phi_k(y)},$ and $\phi_k(y) > m_k.$ Thus the left-hand set is included in the right-hand set.
\end{proof}
