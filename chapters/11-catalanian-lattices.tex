\hypertarget{catalanian-lattices}{%
\chapter{Catalanian lattices}\label{catalanian-lattices}}

This section introduces and proves the author's main results.

\hypertarget{gelfand-tsetlin-patterns}{%
\section{Gelfand-Tsetlin patterns}\label{gelfand-tsetlin-patterns}}

\begin{definition}
A \textit{Gelfand-Tsetlin pattern} or just \textit{GT pattern} of order $n$ is an $n \times n$ lower triangular matrix with integer entries that satisfies $g_{i + 1, j} \leq g_{ij} \leq g_{i +1, j + 1}$ for any $1 \leq j \leq i \leq n.$\footnote{This definition is equivalent to that given in \cite{stanley-ec2} \S7.10. We represent GT patterns as matrices here for ease of computation and typesetting.}
\end{definition}

Visually, this says that entries in a GT pattern weakly decrease when
moving northwest or down.

\begin{definition}
  A GT pattern of order $n$ is \textit{symplectic} if $g_{nj}$ is even for any $j$.
\end{definition}

\begin{definition}
  The \textit{Catalanian lattice of order \(n, k\)} $L_C^{Cat}(n, k) = \mathscr C(n, k)$ is defined as follows.
\begin{enumerate}
\tightlist
\item Vertices of $\mathscr C(n, k)$ are symplectic GT patterns of order $n$ and maximum entry $2k.$
\item $\mathscr C(n, k)$ is ordered componentwise.
\item If $x, y \in \mathscr C(n, k)$, $x \to y$ if and only if $x$ and $y$ differ in exactly one entry $(i, j) \in [n]^2$, with $x_{ij} + 1 = y_{ij}$ if $i < n$ or $x_{ij} + 2 = y_{ij}$ if $i = n.$
\item Edges of $\mathscr C(n, k)$ are colored by $[n]$ according to the value of $i$.
\item Upon concatenating rows (and dividing the final row by 2), $\mathscr C(n, k)$ embeds directly into $\mathbb Z^{\binom n 2}.$
\end{enumerate}
\end{definition}

\begin{figure}[p]
\includegraphics[width=\textwidth]{cat32.png}
\caption{The Catalanian lattice \(\mathscr C(3, 1).\)}
\end{figure}

\hypertarget{the-cardinality-of-l_ccatn-k}{%
\section{\texorpdfstring{The cardinality of
\(L_C^{Cat}(n, k)\)}{The cardinality of L\_C\^{}\{Cat\}(n, k)}}\label{the-cardinality-of-l_ccatn-k}}

\begin{definition}
  The \textit{Catalan sequence}\footnotemark{} \(\{c_n\}_{n=0}^\infty\) is given by \(\displaystyle c_n = \frac1{n+1}\binom{2n}n.\)
\end{definition}

\footnotetext{The Catalan sequence counts, among other things, the number of valid arrangements of $n$ left and $n$ right parentheses; that is, the number of permutations in which every left parenthesis has a matching right parenthesis.}

We shall have cause to represent the Catalan numbers in a slightly
different form:

\begin{proposition}
  \(c_n = 4^n \displaystyle \prod_{k=1}^n \frac{k-1/2}{k+1}.\)
\end{proposition}

\begin{proof}
Hereinafter, define the \textit{double factorial} $n!! \coloneqq \prod_{k=0}^{\lfloor (n-1)/2 \rfloor} (n - 2k) = n\cdot (n-2) \cdot ... \cdot 1.$ We have
  \begin{align*}
c_n = \frac1{n+1}\binom{2n}n &= \frac{(2n)!}{n!(n+1)!} \\
&= \frac{(2n)!!(2n - 1)!!}{n!(n+1)!} \\
&= 2^n \frac{(2n - 1)!!}{(n+1)!} \\
&= 4^n \prod_{k=1}^n \frac{k - 1/2}{k + 1}. \qedhere
  \end{align*}
\end{proof}

In \cite{donnelly-extremal}, it was shown that
\(|L_C^{Cat}(n, 1)| = c_n.\) This section generalizes that result.

\begin{theorem}[M.]
The vertex cardinality of the Catalanian lattice of order \(n, k\) is given by the $k \times k$ Hankel determinant
\begin{align*}
\vert L_c^{Cat}(n, k)\vert = \det M = \begin{vmatrix}
c_n & c_{n+1} & \cdots & c_{n+k-1} \\
c_{n+1} & c_{n+2} & \cdots & c_{n+k} \\
\vdots & \vdots & \ddots & \vdots \\
c_{n+k-1} & c_{n+k} & \cdots & c_{n+2k-2}
\end{vmatrix}
\end{align*}
If we index this matrix by $i, j = 0, 1, \dots, k - 1$, then $m_{ij}$, i.e., the $ij$th entry of $M$, is $c_{n + i + j}.$
\end{theorem}

Before we prove this theorem, we reproduce, more legibly, a result from
\cite{gessel}.

\begin{lemma}
$\displaystyle \det M = \prod_{j=0}^{k-1} \frac{(2n + 2j)!(2j + 1)!}{(n + j + 1)!(k - 1 + n + j)!}.$
\end{lemma}

\begin{proof}
First, note that
\begin{align*}
m_{ij} = c_{n + i + j} &= 4^{n + i + j} \prod_{l = 1}^{n + i + j} \frac{l-1/2}{l+1} \\
&= 4^i 4^{n+j} \left(\prod_{l=1}^{n+j} \frac{l-1/2}{l+1}\right) \cdot \left(\prod_{l=n+j+1}^{n+j+i} \frac{l - 1/2}{l+1}\right) \\
&= 4^i 4^{n+j} \left(\prod_{l=1}^{n+j} \frac{l-1/2}{l+1}\right) \cdot \left(\prod_{l=1}^{i} \frac{n + j + l - 1/2}{n + j + l +1}\right) \\
&= 4^i c_{n+j} \prod_{l=1}^i \frac{n+j+l-1/2}{n+j+l+1}
\intertext{Now consider the $n \times n$ matrix $N$ whose $ij$th entry is}
n_{ij} = \frac{c_{n+i+j}}{4^ic_{n+j}} &= \prod_{l=1}^i \frac{n+j+l-1/2}{n+j+l+1} \\
&= \frac{\Gamma(n+j+i-1/2)\Gamma(n + j + 1)}{\Gamma(n + j - 1/2)\Gamma(n + j + i + 1)}
\intertext{Applying Gauss' hypergeometric identity with $\gamma = n + j + 1$, $\alpha = -i$, $\beta = 3/2$, we have}
n_{ij} &= \sum_{r=0}^{\infty}\prod_{l=0}^{r-1} \frac{(l - i)(l+3/2)}{(l+n+j+1)(l+1)}.
\intertext{Now if $r - 1 - i \geq 0$, the product becomes zero. The maximum value of $i$ is $k - 1$, so the series terminates at or before the point when $r - 1 = k - 1$. Thus it suffices to cut the series off at $r = k - 1$, and we have}
n_{ij} &= \sum_{r=0}^{k-1} \prod_{l=0}^{r-1} \frac{(l-i)(l+3/2)}{(l + n + j + 1)(l+1)} \\
&= \sum_{r=0}^{k-1} \left(\prod_{l=0}^{r-1} \frac1{l+n+j+1}\right) \left(\prod_{l=0}^{r-1} \frac{(l-i)(l + 3/2)}{l + 1} \right).
\end{align*}
Therefore, $N$ can be written as a matrix product $AB$, with $n_{ij} = \sum_r a_{ir} b_{rj}$, where
\begin{align*}
a_{ij} &= \prod_{l=0}^{j-1} \frac{(l-i)(l+3/2)}{l+1}, \\
b_{ij} &= \prod_{l=0}^{j-1} \frac1{l+n+j+1}.
\end{align*}
In particular, we have $\det N = \det A \det B.$ We tackle these determinants separately.

To compute $\det A$, note that $a_{ij} = 0$ if $j > i$; that is, $A$ is lower triangular. Thus
\begin{align*}
\det A = \prod_{i=0}^{k-1} a_{ii} &= \prod_{i=0}^{k-1} \prod_{l=0}^{i-1} \frac{(l - i)(l+ 3/2)}{l + 1} \\
&= \prod_{i=0}^{k-1} \left(\frac{(-1)^i i!}{i!} \prod_{l=0}^{i-1} l + 3/2\right) \\
&= \prod_{i=0}^{k-1} (-1)^i \left(\prod_{l=0}^{i-1} l + 3/2\right) \\
&= \prod_{i=0}^{k-1} (-1)^i 2^{-i} \prod_{i=0}^{k-1} 2l + 3 \\
&= \prod_{i=0}^{k-1} (-1)^i 2^{-i} \frac{(2i + 1)!}{i! \cdot 2^i} \\
&= \prod_{i=0}^{k-1} (-1)^i 4^{-i} \frac{(2i + 1)!}{i!}.
\end{align*}
To compute $\det B$, note that the maximum value taken on by $l$ is $k - 2$, and write
\begin{align*}
b_{ij} = \frac{\prod_{l=j}^{k-2} l + n + i + 1}{\prod_{l=0}^{k-2} l + n + i + 1}.
\end{align*}
If we factor out the denominator from each row $j$, then the matrix $B$ looks like this:
\begin{align*}
B = \left(\prod_{j=0}^{k-1} \frac1{\prod_{l=0}^{k-2} l + n + j + 1}\right)
\begin{bmatrix}
\prod_{l=0}^{k-2} (l + n + 0 + 1) & \cdots & \prod_{l=0}^{k-2} (l + n + k - 2 + 1) \\
\prod_{l=1}^{k-2} (l + n + 0 + 1) & \cdots & \prod_{l=1}^{k-2} (l + n + k - 2 + 1) \\
\vdots & \ddots & \vdots \\
\prod_{l=k-2}^{k-2} (l + n + 0 + 1) & \cdots & \prod_{l=k-2}^{k-2} (l + n + k - 2 + 1) \\
\prod_{l=k-1}^{k-2} (l + n + 0 + 1) & \cdots & \prod_{l=k-1}^{k-2} (l + n + k - 2 + 1)
\end{bmatrix}
\end{align*}
Note that the bottom row is composed of empty products, i.e., 1s, and going up one row adds one additional term to the product. In addition, this product is unchanged across columns, except for the value of $i$. Therefore, subtracting an appropriate multiple of the bottom row from the penultimate row, we obtain $(k + n -1), (k+n), \dots, (2k + n - 3).$ Doing similarly with the antepenultimate row and the bottom two rows, we obtain $(k + n - 1)^2, \dots, (2k + n - 3)^2.$ This process can be repeated for every row without ever changing the determinant. So we conclude that $B$ has the same determinant as
\begin{align*}
B' = \left( \prod_{j=0}^{k-1} \frac1{\prod_{l = 0}^{k - 2} l + n + j + 1}\right)
\begin{bmatrix}
(k + n - 1)^{k - 1} & \cdots & (2k + n - 3)^{k - 1} \\
(k + n - 1)^{k - 2} & \cdots & (2k + n - 3)^{k - 2} \\
\vdots & \ddots & \vdots \\
(k + n - 1) & \cdots & (2k + n - 3) \\
1 & \cdots & 1
\end{bmatrix}.
\end{align*}
The Vandermonde matrix on the right, whose $ij$th entry is $(k + n - 1 + j)^{k - 1 - i}$, has determinant
\begin{align*}
& \prod_{i=0}^{k-1} \prod_{j=i+1}^{k-1} [(k + n - 1 + i) - (k + n - 1 + j)] \\
=& \prod_{i=0}^{k-1} \prod_{j=i+1}^{k-1} (i-j) \\
=& \prod_{i=0}^{k-1} \prod_{j=1}^{k-1-i} (-j) \\
=& \prod_{i=0}^{k-1} (-1)^{k - 1 - i} (k - 1 - i)! \\
=& \prod_{i=0}^{k-1} (-1)^i i!.
\end{align*}
So we conclude that
\begin{align*}
\det B &= \left(\prod_{j=0}^{k-1} \frac1{\prod_{l=0}^{k-2} l + n + j + 1}\right) \cdot \prod_{i=0}^{k-1} (-1)^i i! \\
&= \left(\prod_{j=0}^{k-1} \frac{(n + j)!}{(k - 1 + n + j)!} \right) \cdot \prod_{i=0}^{k-1} (-1)^i i!,
\end{align*}
and thus that
\begin{align*}
\det N  &= \det A \det B \\
&= \left( \prod_{i=0}^{k-1} (-1)^i 4^{-i}\frac{(2i + 1)!}{i!} \right) \cdot \left( \prod_{j=0}^{k-1} \frac{(n + j)!}{(k - 1 + n + j)!}\right) \cdot \prod_{i=0}^{k-1} (-1)^i i! \\
&= \left( \prod_{i=0}^{k-1} (-1)^i 4^{-i}(2i + 1)! \right) \cdot \left( \prod_{j=0}^{k-1} \frac{(n + j)!}{(k - 1 + n + j)!}\right).
\end{align*}
Now to obtain $M$ from $N$, we must multiply each row by $4^i$ and each column by $\displaystyle c_{n+j} = \frac{(2n + 2j)!}{(n+j+1)!(n+j)!}.$ So we finally obtain
\begin{align*}
\det M &= \left( \prod_{i=0}^{k-1} 4^i \right) \cdot \left( \prod_{j=0}^{k-1} \frac{(2n + 2j)! }{(n + j + 1)!(n + j)!}\right) \cdot \det N \\
&= \left( \prod_{i=0}^{k-1}4^i  \right) \cdot \left( \prod_{j=0}^{k-1} \frac{(2n + 2j)! }{(n + j + 1)!(n + j)!}\right) \cdot \left(\prod_{i=0}^{k-1} 4^{-i}(2i+1)! \right) \cdot \left(\prod_{j=0}^{k-1} \frac{(n + j)!}{(k - 1 + n + j)!} \right)\\
&= \left( \prod_{j=0}^{k-1} \frac{(2n + 2j)! }{(n + j + 1)!(k-1 + n + j)!}\right) \cdot \left(\prod_{i=0}^{k-1} (2i+1)! \right)  \\
&= \prod_{j=0}^{k-1} \frac{(2j + 1)!(2n + 2j)!}{(n + j + 1)!(k-1 + n + j)!}. \qedhere
\end{align*}
\end{proof}

\begin{lemma}
$\displaystyle|L_C^{Cat}(n, k)| = \binom{n+k}{k} \prod_{i=1}^n \frac{i!(2k + 2i - 1)!}{(2i - 1)!(2k + i)!}.$
\end{lemma}

\begin{proof}
\cite{donnelly-poset-models}, \S8.10, \S{}B.23 gives
\begin{align*}
\vert L_C^{Cat}(n, k)\vert &= \binom{n+k}{k} \prod_{i=1}^n \prod_{j=0}^{n - i - 1} \frac{2k + 2n + 1 - 2i - j}{2n + 1 - 2i - j} \\
&= \binom{n + k}{k} \prod_{i=1}^n \frac{(n - i + 1)!(2k + 2n - 2i + 1)!}{(2n - 2i + 1)!(2k + n - i + 1)!}.
\intertext{Making the substitution $i' = n - i + 1$, we obtain}
|L_C^{Cat}(n, k)| &= \binom{n+k}{k} \prod_{i=1}^n \frac{i!(2k + 2i - 1)!}{(2i - 1)!(2k + i)!}. \qedhere
\end{align*}
\end{proof}

Therefore, the proof of the foregoing theorem is a matter of showing
that this expression agrees with the one previously obtained for
\(\det M\).

\begin{lemma}
$\displaystyle \prod_{k=1}^N \frac{(2k - 1)!}{(N + k)!} = \prod_{k=1}^N \frac{k!}{(2k)!}.$
\end{lemma}

\begin{proof}
Cross-multiply. Then both sides are equal to $\prod_{k=1}^{2N} k!.$
\end{proof}

\begin{theorem}
For all positive integers $n, k$,
\begin{align*}
\prod_{i=1}^k \frac{(2n + 2i)!(2i - 1)!}{(n + i)!(n + i + k)!} = \binom{n+k}n \prod_{i=1}^n \frac{i!(2k + 2i - 1)!}{(2i - 1)!(2k + i)!}.
\end{align*}
\end{theorem}

\begin{proof}
We proceed by induction on the difference $k - n$ in either direction. There will be two separate inductive cases, each depending on the basis (B) as follows:
\[
\begin{tabular}{l|lllll}
$n$/$k$ & 1 & 2 & 3 & 4 & 5 \\
\hline
1 & B & $\leftarrow$ & $\leftarrow$ & $\leftarrow$ & $\leftarrow$\\
2 & $\rightarrow$ & B & $\leftarrow$ & $\leftarrow$ & $\leftarrow$\\
3 & $\rightarrow$ & $\rightarrow$ & B & $\leftarrow$ & $\leftarrow$\\
4 & $\rightarrow$ & $\rightarrow$ & $\rightarrow$ & B & $\leftarrow$ \\
5 & $\rightarrow$ & $\rightarrow$ & $\rightarrow$ & $\rightarrow$ & B \\
\end{tabular}
\]

\par \textit{Basis:} $k - n = 0$. So $k = n$ and we must show
\begin{align*}
\prod_{i=1}^n \frac{(2n + 2i)!(2i - 1)!}{(n + i)!(2n + i)!} &= \binom{2n}n \prod_{i=1}^n \frac{i!(2n + 2i - 1)!}{(2i - 1)!(2n + i)!}.
\intertext{Applying Lemma 4.3 to the left-hand side, with $N = n$, $k = i$, we obtain}
\prod_{i=1}^n \frac{(2n + 2i)!(2i - 1)!}{(n + i )!(2n + i)!} &= \prod_{i=1}^n \frac{i!(2n + 2i)!}{(2i)!(2n + i)!} \\
&= \prod_{i=1}^n \left( \frac{i!(2n + 2i - 1)!}{(2i - 1)!(2n+i)!} \cdot \frac{2n + 2i}{2i} \right) \\
&= \prod_{i=1}^n \frac{i!(2n + 2i - 1)!}{(2i - 1)!(2n+i)!} \cdot \prod_{i=1}^n \frac{n + i}{i} \\
&= \binom{2n}n \prod_{i=1}^n \frac{i!(2n + 2i - 1)!}{(2i - 1)!(2n+i)!},
\end{align*}
which was to be shown.
\par \textit{Inductive case 1:} $k - n > 0$. Assume inductively that the theorem holds for $k = n+ r$, $r = 0, 1, \dots$. Explicitly, assume
\begin{align*}
\prod_{i=1}^{n+r} \frac{(2n + 2i)!(2i - 1)!}{(n + i)!(2n + i + r)!} &= \binom{2n + r}{n} \prod_{i=1}^n \frac{i!(2n+ 2r+ 2i - 1)!}{(2i - 1)!(2n + 2r + i)!}.
\intertext{We must show that the theorem holds for $k = n + r + 1$; explicitly, that}
\prod_{i=1}^{n+r + 1} \frac{(2n + 2i)!(2i - 1)!}{(n + i)!(2n + i + r + 1)!} &= \binom{2n + r + 1}{n} \prod_{i=1}^n \frac{i!(2n+ 2r+ 2i + 1)!}{(2i - 1)!(2n + 2r + i + 2)!}.
\end{align*}
By the inductive hypothesis, it suffices to show that the quotient of the $k = n + r + 1$ case by the $k = n+r$ case on both sides is equal.

On the left, we have
\begin{align*}
&\frac{(4n + 2r+ 2)!(2n+ 2r+ 1)!}{(2n + r+ 1)!} \cdot \frac{\prod_{i=1}^{n + r + 1} (1/2n + i+ r + 1)!)}{\prod_{i=1}^{n+r} (2n + i + r + 1)!} \\
=~&\frac{(4n + 2r + 2)!(2n+ 2r+ 1)!}{(2n + r+ 1)!(3n + 2r + 2)!} \cdot \frac{\prod_{i=1}^{n + r} (2n + i +r)!}{\prod_{i=1}^{n+r} (2n + i + r + 1)!} \\
=~&\frac{(4n + 2r + 2)!(2n + 2r + 1)!}{(2n + r + 1)!(3n + 2r + 2)!} \cdot \prod_{i=1}^{n+r} \frac1{2n + i + r + 1} \\
=~&\frac{(4n + 2r + 2)!(2n + 2r + 1)!}{(2n + r + 1)!(3n + 2r + 2)!} \cdot \frac{(2n + r + 1)!}{(3n + 2r + 1)!} \\
=~&\frac{(4n + 2r + 2)!(2n + 2r + 1)!}{(3n + 2r + 1)!(3n + 2r + 2)!}.
\end{align*}

On the right, we have
\begin{align*}
&\frac{\binom{2n + r + 1}{n}}{\binom{2n + r}{n}} \cdot \prod_{i=1}^n \prod_{i=1}^n \frac{(2n + 2r+ 2i + 1)!(2n + 2r + i)!}{(2n + 2r + 2i - 1)!(2n + 2r + i + 1)!} \\
=~& \frac{2n + r + 1}{n + r + 1} \cdot \prod_{i=1}^n \frac{(2n + 2r+ 2i + 1)(2n + 2r + 2i)}{(2n + 2r+ i + 2)(2n + 2r + i + 1)}
\intertext{Turning the terms depending on $2i$ into double factorials and the other terms into single factorials, we have}
=~& \frac{2n + r + 1}{n + r + 1} \cdot \frac{(4n + 2r + 1)!! (4n + 2r)!!}{(2n + 2r + 1)!!(2n + 2r)!!} \cdot \frac{(2n + 2r + 2)!(2n + 2r + 1)!}{(3n + 2r + 2)!(3n + 2r + 1)!} \\
=~& \frac{2n + r + 1}{n + r + 1} \cdot \frac{(4n + 2r + 1)!}{(2n + 2r + 1)!} \cdot \frac{(2n + 2r + 2)!(2n + 2r + 1)!}{(3n + 2r + 2)!(3n + 2r + 1)!} \\
=~& \frac{2n + r + 1}{n + r + 1} \cdot \frac{(4n + 2r + 1)!(2n + 2r + 2)!}{(3n + 2r + 2)!(3n + 2r + 1)!} \\
=~& \frac{4n + 2r + 2}{2n + 2r + 2} \cdot \frac{(4n + 2r + 1)!(2n + 2r + 2)!}{(3n + 2r + 2)!(3n + 2r + 1)!} \\
=~& \frac{(4n + 2r + 2)!(2n + 2r + 1)!}{(3n + 2r + 2)!(3n + 2r + 1)!}.
\end{align*}
This completes the first inductive case.

\textit{Inductive case 2:} $k - n < 0$. This proceeds analogously to the previous inductive case, except the induction runs backwards.

Assume inductively that the theorem holds for $k = n - r$, $r = 0, 1, \dots, n - 2$. Explicitly, assume
\begin{align*}
\prod_{i=1}^{n-r} \frac{(2n + 2i)!(2i - 1)!}{(n + i)!(2n + i - r)!} &= \binom{2n - r}{n} \prod_{i = 1}^n \frac{i!(2n - 2r + 2i - 1)!}{(2i - 1)!(2n - 2r + i)!}
\intertext{We must show that the theorem holds for $k = n - r - 1$; explicitly,}
\prod_{i=1}^{n-r - 1} \frac{(2n + 2i)!(2i - 1)!}{(n + i)!(2n + i - r - 1)!} &= \binom{2n - r - 1}{n} \prod_{i = 1}^n \frac{i!(2n - 2r + 2i - 3)!}{(2i - 1)!(2n - 2r + i - 2)!}
\end{align*}
As before, we show that the quotient of the $n - r$ case by the $n - r - 1$ case is the same on both sides.

On the left, we have:
\begin{align*}
&\frac{(4n - 2r)!(2n - 2r - 1)!}{(2n - r)!} \cdot \frac{\prod_{i=1}^{n - r} (1/(2n + i - r)!)}{\prod_{i=1}^{n - r - 1} (1 / (2n + i - r - 1)!)} \\
=~&\frac{(4n - 2r)!(2n - 2r - 1)!}{(2n - r)!} \cdot \frac{\prod_{i=1}^{n - r - 1} (2n + i - r - 1)!}{\prod_{i=1}^{n - r} (2n + i - r)!} \\
=~&\frac{(4n - 2r)!(2n - 2r - 1)!}{(2n - r)!(3n - 2r)!} \cdot \frac{\prod_{i=1}^{n - r - 1} (2n + i - r - 1)!}{\prod_{i=1}^{n - r - 1} (2n + i - r)!} \\
=~&\frac{(4n - 2r)!(2n - 2r - 1)!}{(2n - r)!(3n - 2r)!} \cdot \prod_{i=1}^{n - r - 1} \frac{1}{2n + i - r} \\
=~&\frac{(4n - 2r)!(2n - 2r - 1)!}{(2n - r)!(3n - 2r)!} \cdot \frac{(2n - r)!}{(3n - 2r - 1)!} \\
=~&\frac{(4n - 2r)!(2n - 2r - 1)!}{(3n - 2r)!(3n - 2r - 1)!}.
\end{align*}

And on the right, we have
\begin{align*}
& \frac{\binom{2n - r}n}{\binom{2n - r  - 1}{n}} \cdot \prod_{i=1}^n \frac{(2n - 2r + 2i  - 1)!(2n - 2r + i - 2)!}{(2n - 2r + 2i - 3)!(2n - 2r + i)!} \\
=~& \frac{2n - r}{n - r}\cdot \prod_{i=1}^n \frac{(2n - 2r + 2i - 1)(2n - 2r + 2i - 2)}{(2n - 2r + i)(2n - 2r + i - 1)} \\
=~& \frac{2n - r}{n - r} \cdot \frac{(4n - 2r - 1)!!(4n - 2r - 2)!!}{(2n - 2r - 1)!!(2n - 2r - 2)!!} \cdot \frac{(2n - 2r)!(2n - 2r - 1)!}{(3n - 2r)!(3n - 2r - 1)!} \\
=~& \frac{2n - r}{n - r} \cdot \frac{(4n - 2r - 1)!}{(2n - 2r - 1)!} \cdot \frac{(2n - 2r)!(2n - 2r - 1)!}{(3n - 2r)!(3n - 2r - 1)!} \\
=~& \frac{2n - r}{n - r} \cdot \frac{(4n - 2r - 1)!(2n - 2r)!}{(3n - 2r)!(3n - 2r - 1)!} \\
=~& \frac{4n - 2r}{2n - 2r} \cdot \frac{(4n - 2r - 1)!(2n - 2r)!}{(3n - 2r)!(3n - 2r - 1)!} \\
=~& \frac{(4n - 2r)!(2n - 2r - 1)!}{(3n - 2r)!(3n - 2r - 1)!}
\end{align*}
This completes the proof.
\end{proof}

\hypertarget{uniqueness-of-dcs-coefficients}{%
\section{Uniqueness of DCS
coefficients}\label{uniqueness-of-dcs-coefficients}}

In this section, we show the following result.

\begin{theorem}[Dunkum-Donnelly-Malone]
If there exist a set of DCS-satisfactory coefficients for the lattice $\mathscr C(n, k)$, they are uniquely determined and positive rational.
\end{theorem}

Key to this result is a lemma appearing in \cite{gelfand-tsetlin}.

\begin{lemma}[Gelfand-Tsetlin 1950]
A color-$k$ component of $\mathscr C(n, k)$, $k = 1, \dots, n - 1$, realizes a representation of the Lie algebra $A_{k + 1}.$ Furthermore, the edge coefficients are positive rationals.
\end{lemma}

\begin{proof}[Proof of Theorem 4.3.]
By Lemma 4.4, there exist uniquely determined, DCS-satisfactory, positive rational coefficients for the components of color $1, \dots, n - 1.$

We induct over color-$n$ components. To induct, we must assign an integer $s(C)$ to each color-$n$ component. Observe that all vertices of a given color-$n$ component differ only in the $n$th row. Therefore, let $s(C)$ be the sum of the entries in rows $1$ through $n - 1.$

\textit{Basis, $s(C) = 0.$} In this case, the $n$th row must be all zeros, with the exception of the bottom right entry, which must be one of $0, 2, \dots, 2k.$ So $C$ is a chain of length $k$, and its edge coefficients are uniquely determined positive rational numbers, as shown previously.

Now assume inductively that for some nonnegative integer $S$, if $C$ is a color-$n$ component and $s(C) \leq S$, then the edges of $C$ have uniquely determined, DCS-satisfactory, positive rational coefficients. Suppose we have a component $C$ with $s(C) = S + 1.$ Consider an edge $x \overset{n}\to y,$ where $x_{nj}+2 = y_{nj}$ for some $j \in [n].$ We consider two cases.
\begin{enumerate}
\tightlist
\item Some entry $y_{i', j'}$ is decrementable in $y,$ where $1 \leq i' \leq n - 1$; that is, subtracting one from $y_{i',j'}$ also gives a valid symplectic GT pattern $y'$. Then $x_{i', j'}$ is also decrementable, since every entry of $x$ is no larger than the corresponding entry of $y.$

Therefore there are edges $y' \overset{i'}\to y$, $x' \overset{i'}\to x,$ whose coefficients are uniquely determined as positive rational numbers by Lemma 4.4. By distributivity, there is an edge $x' \overset{n}\to y',$ belonging to an $n$-component $C'$ with $s(C') = S.$ Its coefficient is uniquely as a positive rational number by the inductive assumption. Thus the coefficient of $x \overset{n} \to y$ is uniquely determined as a positive rational number by the diamond relation at $x', x, y', y.$

\item No entry $y_{i', j'}$ is decrementable in $y,$ where $i \leq i' \leq n - 1.$ Then $y$ is the maximum element of an $n$-component, since if some entry $y_{n, j''}$ were incrementable, the entry above it $y_{n - 1, j''}$ would be decrementable.

Therefore the only edges incident to $y$ have color $n$. If the only such edge is $x \overset{n}\to y$, the coefficient of $y$ is immediately determined uniquely as a positive rational number by the color-$n$ crossing relation at $x.$

Suppose there are at least two color-$n$ edges incident to $y$. One of them is $x \overset{n} \to y.$ For any other edge $x' \overset{n} \to y$, distributivity forces a diamond of edges $y' \overset{n} \to x \overset{n} \to y$, $y' \overset{n} \to x' \overset{n} \to y,$ where $y' = x \wedge x'.$ Since $x, x'$ are nonmaximal, the edges $y' \overset{n} \to x$ and $y' \overset{n} \to x'$ have uniquely determined, positive rational coefficients, by case 1. By the crossing relation at $x'$, the coefficient of the edge $x' \overset{n} \to y$ is uniquely determined and rational.

We claim each of the coefficients of the edges $x' \overset{n} \to y$ is positive. Two such edges cannot have differing signs because, by their diamond relation, their product must be positive. For the same reason, no edge coefficient can be zero. So the coefficients are all positive or all negative. But their sum gives the color-$n$ weight of $y$, which is positive since $y$ is maximum. Therefore they must be positive. \qedhere
\end{enumerate}
\end{proof}
