* Paths and tableaux
  :PROPERTIES:
  :CUSTOM_ID: motzkin-paths
  :END:

** Motzkin paths

\begin{definition}
A \textit{topside peakless Motzkin path} of length $n$ is a sequence $\{p_i\}_{i=1}^n$ such that for each $1 \leq i \leq n,$
\begin{enumerate}
\tightlist
\item $p_i \in \{-1, 0, 1\};$
\item $\sum_{k=1}^i p_k \geq 0;$
\item if $p_{i - 1} = 1$, $p_i \not = -1,$
\end{enumerate}
and for which $\sum_{i=1}^n p_i = 0.$
\end{definition}

By calling $\{p_i\}_{i=1}^n$ a ``path,'' we mean to identify it with the series of points $\{(i, \sum_{k=1}^i p_k)\}_{i=1}^n$. The above definition then says that the path goes from $(0, 0)$ to $(n, 0)$; that it remains on or above the $x$-axis (/topside/); and that no step $\nearrow$ is immediately followed by a step $\searrow$ (/peakless/).

\begin{figure}
\centering
\subfloat{%
\begin{pspicture}(0,0)(4,2)
\psdot*(0,0)
\psline[linewidth=1pt]{->}(0,0)(1,0) \psdot*(1,0)
\psline[linewidth=1pt]{->}(1,0)(2,0) \psdot*(2,0)
\psline[linewidth=1pt]{->}(2,0)(3,0) \psdot*(3,0)
\psline[linewidth=1pt]{->}(3,0)(4,0) \psdot*(4,0)
\end{pspicture}
}\hspace{0.3\textwidth}%
\subfloat{%
\begin{pspicture}(0,0)(4,2)
\psdot*(0,0)
\psline[linewidth=1pt]{->}(0,0)(1,0) \psdot*(1,0)
\psline[linewidth=1pt]{->}(1,0)(2,1) \psdot*(2,1)
\psline[linewidth=1pt]{->}(2,1)(3,1) \psdot*(3,1)
\psline[linewidth=1pt]{->}(3,1)(4,0) \psdot*(4,0)
\end{pspicture}
}\\
\subfloat{%
\begin{pspicture}(0,0)(4,2)
\psdot*(0,0)
\psline[linewidth=1pt]{->}(0,0)(1,1) \psdot*(1,1)
\psline[linewidth=1pt]{->}(1,1)(2,1) \psdot*(2,1)
\psline[linewidth=1pt]{->}(2,1)(3,0) \psdot*(3,0)
\psline[linewidth=1pt]{->}(3,0)(4,0) \psdot*(4,0)
\end{pspicture}
}\hspace{0.3\textwidth}%
\subfloat{%
\begin{pspicture}(0,0)(4,2)
\psdot*(0,0)
\psline[linewidth=1pt]{->}(0,0)(1,1) \psdot*(1,1)
\psline[linewidth=1pt]{->}(1,1)(2,1) \psdot*(2,1)
\psline[linewidth=1pt]{->}(2,1)(3,1) \psdot*(3,1)
\psline[linewidth=1pt]{->}(3,1)(4,0) \psdot*(4,0)
\end{pspicture}
}
\caption{The four topside peakless Motzkin paths of length $4$.}
\end{figure}

\begin{proposition}
Denote the number of topside peakless Motzkin paths of length $n$ by $P_n.$ Then we have the recurrence
\begin{align*}
P_n = \begin{cases} 1,& n = 0 \\ P_{n-1} + \sum_{i=3}^n P_{i-2}P_{n - i},  & n = 1, 2, \dots\end{cases}.
\end{align*}
\end{proposition}
\begin{proof}
It is obvious that $P_0 = 1$. Consider an arbitrary path of length $n > 0$. Consider the first index $i$ at which $\sum_{k=1}^i p_k = 0$, and let $P_n(i)$ be the number of length-$n$ paths for which this occurs given some choice of $i$. Clearly $i$ is well-defined for any path, so $P_n = \sum_{i=1}^n P_n(i).$
\begin{enumerate}
\tightlist
\item If $i = 1$, then $p_1 = 0$, and $\{p_k\}_{k=2}^n$ is a valid path of length $n - 1$, so $P_n(1) = P_{n - 1}.$
\item $P_n(2) = 0.$ (Why?)
\item Otherwise, $ 3 \leq i \leq n$, and the subpaths $Q = \{p_k\}_{k=1}^i$ and $R = \{p_k\}_{k=i+1}^n$ are themselves two valid paths of lengths $i$ and $n - i$ respectively. Since by hypothesis $i$ was the \textit{first} index at which $\sum_{k=1}^i p_k = 0$, $Q' = \{p_k\}_{k=2}^{i-1}$ is \textit{also} a valid path of length $i - 2$. Since the choices of $Q'$ and $R$ are completely free, we have $P_n(i) = P_{i-2}P_{n - i}.$
\end{enumerate}
This completes the proof.
\end{proof}

The associated sequence is A004148 in the OEIS \cite{oeis-motzkin} and runs 1, 1, 1, 2, 4, 8, 17, 37, ...

** Littlewood-Richardson tableaux

\begin{definition}
Let $P, Q \in \mathbb Z^n$ be weakly decreasing $n$-tuples of nonnegative integers with $p_i > q_i$ for each $i \in [n].$ A \textit{Littlewood-Richardson tableau} of shape $P/Q$ is an assignment of $1$, $2$, or $3$ to each empty square in a diagram such as the following satisfying the following conditions:
\begin{enumerate}
\tightlist
\item each row is weakly increasing, read left to right;
\item each column is strongly increasing, read top to bottom;
\item when following the diagram by rows, \textit{right to left,} top to bottom, it is always true that the count of 1s is at least the count of 2s is at least the count of 3s.
\end{enumerate}
\begin{figure}
\centering
\begin{ytableau}
\none & \none & \none & & &\\
\none & \none & & \\
\none & \none & \none[\vdots] \\
\none & & \\
&
\end{ytableau}
\caption{An example (unfilled) tableau.}
\end{figure}
(There are $p_i$ total spaces on the $i$th row, $q_i$ of which are unfilled.)
\end{definition}

We will mainly be concerned with tableaux of the shape \((n, n-1, n-3, \dots)/(n - 2, n - 4, \dots)\). (The dots indicate that the shape vector entries decrease to zero rather than becoming negative.)

\begin{figure}
\centering
\subfloat{%
\begin{ytableau}
\none & \none & 1 & 1 \\
1 & 1 & 2 \\
2
\end{ytableau}
}\hfill%
\subfloat{%
\begin{ytableau}
\none & \none & 1 & 1 \\
1 & 1 & 2 \\
3
\end{ytableau}
}\hfill%
\subfloat{%
\begin{ytableau}
\none & \none & 1 & 1 \\
1 & 2 & 2 \\
2
\end{ytableau}
}\hfill%
\subfloat{%
\begin{ytableau}
\none & \none & 1 & 1 \\
1 & 2 & 2 \\
3
\end{ytableau}
}
\caption{The four valid LR tableaux for $n = 4.$}
\end{figure}

** Equinumeracy?

The alert reader will notice that the sequence that counts peakless Motzkin paths also apparently counts LR tableaux of the shape \((n,n-1,n-3)/(n-2,n-4,\dots).\)

\begin{conjecture}
For any $n \geq 0$, the number of LR tableaux of the given shape is given by \(P_n.\)
\end{conjecture}

This is confirmed computationally (see Chapter 6) up to at least $n = 26$ (at which both sequences number \(560954047.\))

One possible attack on this conjecture is as follows. Define the \textit{maximal prefix} of any LR tableau to be the largest $n$ such that the first $1, 2, \dots, n$ columns of the tableau (read from left to right) themselves form a valid tableau. Necessarily, if the maximal prefix is not $n$, taking the first $n + 1$ columns must violate one of the LR conditions.

The reader may verify that the tableaux in Figure 5.3 above, from left to right, have maximal prefixes $4$, $0$, $1$, and $0$. It is clear that exactly one tableau, the one given by taking the smallest allowable choice for each number, has maximal prefix $n$. Observe that exactly $2 = 4 - 2 = P_4 - P_3$ tableaux have maximal prefix $0$; exactly $1 = P_3 - P_2$ tableau has maximal prefix $1$; and $0 = P_2 - P_1 = P_1 - P_0$ tableaux have maximal prefix $2$ or $3$.

\begin{conjecture}
The number of $n$-column LR tableaux having maximal prefix $k = 0, 1, \dots, n - 1$ is given by \(P_{n - k} - P_{n - k - 1}.\)
\end{conjecture}

Since the maximal prefix is well-defined, this would immediately imply Conjecture 5.1. There is also computational evidence to support this pattern.

Since we have \(P_{n - k} - P_{n - k - 1} = \sum_{i=3}^{n-k} P_{i-2}P_{n - k - i},\) a characterization of a tableau having maximal prefix $k$ as a concatenation of two freely chosen tableaux of the appropriate lengths, in the spirit of Proposition 5.1, would suffice.
