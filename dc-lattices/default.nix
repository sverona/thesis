let pkgs = import <nixpkgs> {};
in pkgs.mkShell {
	buildInputs = with pkgs.python3Packages; [
		black
		numpy
		matplotlib
		scipy
		networkx
	];
}
