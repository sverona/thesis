#!/usr/bin/env python3


def is_lower_triangular(mat):
    for row_idx, row in enumerate(mat):
        for col_idx, entry in enumerate(row):
            if (
                col_idx > row_idx
                and entry is not None
                and entry != 0
            ):
                return False
    return True


def is_positive_and_under_cap(mat, cap=2):
    for row_idx, row in enumerate(mat):
        for col_idx, entry in enumerate(row):
            if entry is not None and not (
                0 <= entry <= cap
            ):
                return False
    return True


def is_symplectic(mat):
    for col_idx, entry in enumerate(mat[-1]):
        if entry is not None and entry % 2 != 0:
            return False
    return True


def has_gt_property(mat):
    for row_idx, row in enumerate(mat):
        if row_idx == len(mat) - 1:
            continue
        for col_idx, entry in enumerate(row):
            if col_idx > row_idx:
                continue
            south_of_us = mat[row_idx + 1][col_idx]
            se_of_us = mat[row_idx + 1][col_idx + 1]
            if None in [entry, south_of_us, se_of_us]:
                continue
            if not (south_of_us <= entry <= se_of_us):
                return False
    return True


def is_gt(mat, cap=2):
    return (
        is_positive_and_under_cap(mat, cap=cap)
        and is_symplectic(mat)
        and is_lower_triangular(mat)
        and has_gt_property(mat)
    )


def is_complete(mat):
    for row in mat:
        if None in row:
            return False
    return True


def first_incrementable_entry(mat):
    for row_idx, row in enumerate(mat):
        for col_idx, entry in enumerate(row):
            if entry is None:
                return [row_idx, col_idx]
    return None


def backtrack(candidate, cap=2):
    if not is_gt(candidate, cap=cap):
        return
    elif is_complete(candidate):
        yield candidate

    first_increment = first_incrementable_entry(candidate)

    if first_increment is None:
        return
    else:
        (
            first_increment_row,
            first_increment_col,
        ) = first_increment
        for val in range(cap + 1):
            copy_of_candidate = [
                [x for x in row] for row in candidate
            ]
            copy_of_candidate[first_increment_row][
                first_increment_col
            ] = val
            yield from backtrack(copy_of_candidate, cap=cap)


def gt_patterns(size, cap=2):
    initial = [
        [0 if col > row else None for col in range(size)]
        for row in range(size)
    ]

    yield from backtrack(initial, cap=cap)
