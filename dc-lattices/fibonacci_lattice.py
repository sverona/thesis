#!/usr/bin/env python3

"""Computationally models the Fibonaccian lattice $$L_A^{Fib}(n + 1, k).$$
Throughout, we refer to the parameters $$n + 1$$, $$k$$ as the _width_ and _length_.
"""

from itertools import product, starmap

from incremental_lattice import IncrementalLattice


class FibonacciLattice(IncrementalLattice):
    def __init__(self, n, k):
        self.width = n
        self.length = k

        super().__init__()

    def generate_vertices(self):
        r"""Return all vertices in this lattice.
        A vertex of $$L_A^{Fib}(n + 1, k)$$ is a $$k$$-tuple $$v \in \mathbb Z^k$$ satisfying

          - $$(i - 1) < v_i / k \leq i $$;
          - $$v_{i + 1} - v_i > 1 $$ for each $$i = 1, dots, k - 1$$.
        """

        min_tab = [
            1 + self.width * i for i in range(self.length)
        ]
        offsets = product(
            range(self.width), repeat=self.length
        )

        for offset in offsets:
            tab = tuple(
                map(lambda c, o: c + o, min_tab, offset)
            )

            if self.is_valid_label(tab):
                yield tab

    def colors(self):
        return range(1, self.width)

    def edge_color(self, vertex1, vertex2):
        if not self.is_adjacent(vertex1, vertex2):
            return None

        diffs = [
            (comp1, comp2)
            for comp1, comp2 in zip(vertex1, vertex2)
            if comp1 != comp2
        ][0]

        lower_label = min(diffs)

        # The following variables refer to this zero-indexed grid.
        # 1   n-1 1   n-1 (k columns)
        # 2   n-2 2   n-2 ...
        # ...
        # n-1 1   n-1 1
        # The color is given by the element in column lower_label // n
        # and row lower_label % n.

        col = lower_label // self.width
        row = lower_label - self.width * col
        if col % 2 == 0:
            return row
        return self.width - row

    def is_valid_label(self, tableau):
        """Check if `tableau` is a valid vertex for this lattice.
        """

        for idx in range(1, len(tableau)):
            if tableau[idx] - tableau[idx - 1] == 1:
                return False

        for idx, coord in enumerate(tableau):
            if (
                not idx * self.width
                < coord
                <= (1 + idx) * self.width
            ):
                return False

        return True
