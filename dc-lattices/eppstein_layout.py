import numpy as np
import networkx as nx


def dot_product(a, b, range_=None):
    if not range_:
        range_ = range(len(a))
    return sum(a[k] * b[k] for k in range_)


def eppstein_layout(G, coordinates=np.concatenate):
    X, Y = eppstein_projection(G, coordinates)

    return {
        v: tuple(
            [
                dot_product(X, coordinates(v)),
                dot_product(Y, coordinates(v)),
            ]
        )
        for v in G
    }


def eppstein_projection(G, coordinates=np.concatenate):
    def possible_entries(G, i):
        return [coordinates(v)[i] for v in G]

    def slice(G, i, j):
        for v in G:
            if coordinates(v)[i] == j:
                yield v

    for v in G:
        d = len(coordinates(v))
        break
    X = [None for _ in range(d)]
    Y = [None for _ in range(d)]

    X[0] = 0
    for i in range(1, d):
        current_max = None
        for j in sorted(possible_entries(G, i)):
            this_slice = list(slice(G, i, j))
            last_slice = list(slice(G, i, j - 1))
            if len(this_slice) == 0 or len(last_slice) == 0:
                continue
            candidate_max_min = min(
                dot_product(coordinates(v), X, range(i))
                for v in this_slice
            )
            candidate_max_max = max(
                dot_product(coordinates(v), X, range(i))
                for v in last_slice
            )
            candidate_max = (
                candidate_max_max - candidate_max_min + 1
            )
            if (
                current_max is None
            ) or candidate_max > current_max:
                current_max = candidate_max
        X[i] = current_max

    Y[-1] = 0
    y_range = list(range(d - 1))[::-1]
    for i in y_range:
        current_max = None
        for j in sorted(possible_entries(G, i)):
            this_slice = list(slice(G, i, j))
            last_slice = list(slice(G, i, j - 1))
            if len(this_slice) == 0 or len(last_slice) == 0:
                continue
            candidate_max_min = min(
                dot_product(
                    coordinates(v), X, range(i + 1, d)
                )
                for v in this_slice
            )
            candidate_max_max = max(
                dot_product(
                    coordinates(v), X, range(i + 1, d)
                )
                for v in last_slice
            )
            candidate_max = (
                candidate_max_max - candidate_max_min + 1
            )
            if (
                current_max is None
            ) or candidate_max > current_max:
                current_max = candidate_max
        Y[i] = current_max

    return (X, Y)
