def columns_are_strongly_increasing(tab):
    for col in tab:
        if None in col:
            continue
        if len(col) == 2 and not (col[0] < col[1]):
            return False
    return True


def rows_are_weakly_decreasing(tab):
    if len(tab) < 2:
        return True

    if len(tab) % 2 == 0:
        num_rows_to_check = len(tab) // 2
    else:
        num_rows_to_check = len(tab) // 2 + 1

    for row_idx in range(num_rows_to_check):
        if row_idx == 0:
            row = [tab[0][0], tab[1][0]]
        elif row_idx * 2 + 1 == len(tab):
            row = [tab[-2][1], tab[-1][0]]
        else:
            row = [
                tab[row_idx * 2 - 1][1],
                tab[row_idx * 2][0],
                tab[row_idx * 2 + 1][0],
            ]

        if None in row:
            continue

        for n in range(len(row) - 1):
            if not (row[n] >= row[n + 1]):
                return False
    return True


def is_lattice_word(tab):
    frequencies = [0, 0, 0]
    for col in tab:
        for entry in col:
            if entry is not None:
                frequencies[entry - 1] += 1

            if not (
                frequencies[0]
                >= frequencies[1]
                >= frequencies[2]
            ):
                return False
    return True


def is_lr(tab):
    return (
        columns_are_strongly_increasing(tab)
        and rows_are_weakly_decreasing(tab)
        and is_lattice_word(tab)
    )


def first_incrementable_entry(tab):
    for col_idx, col in enumerate(tab):
        if None in col:
            return tuple([col_idx, col.index(None)])
    return None


def is_complete(tab):
    for col_idx, col in enumerate(tab):
        if None in col:
            return False
    return True


def backtrack(candidate):
    if not is_lr(candidate):
        return
    elif is_complete(candidate):
        yield candidate

    first_increment = first_incrementable_entry(candidate)
    if first_increment is None:
        return
    else:
        (
            first_increment_row,
            first_increment_col,
        ) = first_increment

        for val in [1, 2, 3]:
            copy_of_candidate = [
                [x for x in row] for row in candidate
            ]
            copy_of_candidate[first_increment_row][
                first_increment_col
            ] = val
            yield from backtrack(copy_of_candidate)


def lr_tableaux(size):
    columns = [1 + x % 2 for x in range(size)]
    initial = [
        [None for _ in range(col)] for col in columns
    ]
    yield from backtrack(initial)
