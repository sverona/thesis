#!/usr/bin/env python3

from itertools import product

from incremental_lattice import IncrementalLattice


class BooleanLattice(IncrementalLattice):
    def __init__(self, n):
        self.n = n

        super().__init__()

    def generate_vertices(self):
        yield from product([0, 1], repeat=self.n)

    def colors(self):
        return range(self.n)

    def is_valid_label(self, potential_child):
        return len(potential_child) == self.n and all(
            i in [0, 1] for i in potential_child
        )

    def edge_color(self, vertex1, vertex2):
        return min(
            i
            for i in range(len(vertex1))
            if vertex1[i] != vertex2[i]
        )
