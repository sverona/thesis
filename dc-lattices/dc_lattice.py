#!/usr/bin/env python3

""" Computationally models generic diamond-crossing ("DC") lattices.
"""
from itertools import combinations

import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import networkx as nx


class DCLattice(nx.Graph):
    def __init__(self):
        super().__init__()

        self.add_edges_from(self.generate_edges())

    def generate_vertices(self):
        """Should be overridden.
        """
        pass

    def potential_children(self, vertex):
        """Should be overridden.
        """
        pass

    def generate_edges(self):
        """Generate all edges in this lattice.
        """
        for vertex in self.generate_vertices():
            for potential_child in self.potential_children(
                vertex
            ):
                if self.is_valid_label(potential_child):
                    yield (
                        vertex,
                        potential_child,
                        {
                            "color": self.edge_color(
                                vertex, potential_child
                            )
                        },
                    )

    def edge_color(self, vertex1, vertex2):
        """Return the color the edge between `vertex1` and `vertex2` should have, or None
        if they are nonadjacent.

        Should be overridden.
        """
        pass

    def solvable_from(self, edge):
        sc = self.solvable_subgraph(edge)

        remaining_nodes = self.nodes() - sc.nodes()
        remaining_edges = self.edges() - sc.edges()
        solvable = not (remaining_nodes or remaining_edges)

        if not solvable:
            print("Remaining vertices:")
            for node in remaining_nodes:
                print("\t", node)
            print("Remaining edges:")
            for edge in remaining_edges:
                print("\t", edge)

        return solvable

    def solvable_subgraph(self, edge, return_order=False):
        total_edges = len(self.edges())
        solvable_edges = []

        Q = []

        unsolved_diamonds = list(self.diamonds())

        def push(Q, edge):
            edge = (min(edge), max(edge))

            if edge not in Q and edge not in solvable_edges:
                return [edge] + Q
            else:
                return Q

        if edge:
            Q = push(Q, edge)

        for chain in self.chains():
            for edge in chain.edges():
                # print("Pushing chain edge", edge)
                Q = push(Q, edge)

        while Q:
            """
            print("Q is:")
            for edge in Q:
                print(edge)
            print()

            print("solvable is:")
            for edge in solvable_edges:
                print(edge)
            print()
            """
            edge = Q.pop()
            # print("Popping", edge, len(solvable_edges), "/", total_edges, len(unsolved_diamonds))
            solvable_edges.append(edge)

            # Check if edge's component is solvable.
            component = self.component(
                min(edge), self.edge_color(*edge)
            )

            def component_is_solvable_now(component):
                for face in self.faces(component):
                    edges = face.edges()
                    edges = [
                        (min(edge), max(edge))
                        for edge in edges
                    ]

                    if not set(edges) - set(solvable_edges):
                        return True
                return False

            if component_is_solvable_now(component):
                for edge in component.edges():
                    # print("Pushing solvable component edge", edge)
                    Q = push(Q, edge)

            # Check for solvable diamonds.
            for diamond in unsolved_diamonds:
                top, left, right, bottom = diamond
                diamond_edges = set(
                    (
                        (top, left),
                        (top, right),
                        (left, bottom),
                        (right, bottom),
                    )
                )

                unsolved_edges = list(
                    diamond_edges - set(solvable_edges)
                )

                if len(unsolved_edges) == 1:
                    # print("Pushing lone diamond edge", edge)
                    Q = push(Q, unsolved_edges[0])
                    unsolved_diamonds.remove(diamond)

        if return_order:
            return solvable_edges

        return nx.Graph(solvable_edges)

    def colors(self):
        """Should be overridden.
        """
        pass

    def diamonds(self):
        for vertex in self:
            yield from self.diamonds_at_vertex(vertex)

    def diamonds_at_vertex(self, vertex):
        covers = [
            nbr for nbr in self[vertex] if nbr > vertex
        ]
        for left, right in combinations(covers, 2):
            yield (
                vertex,
                left,
                right,
                self.meet(left, right),
            )

    def meet(self, vertex, neighbor):
        """Should be overridden.
        """
        pass

    def join(self, vertex, neighbor):
        """Should be overridden.
        """
        pass

    def coordinates(self, vertex):
        return vertex

    def chains(self):
        """Return an iterator over all chains in this lattice.

        A color-component is a chain iff its vertex cardinality exceeds its
        edge cardinality by one.
        """

        chain_list = set()

        for vertex in self:
            for color in self.colors():
                component = self.component(vertex, color)
                if (
                    len(component)
                    == len(component.edges()) + 1
                ):
                    chain_list.add(component)

        return chain_list

    def breadth_first_search(self, node, key, graph=None):
        if not graph:
            graph = self

        found = set()
        found_edges = []
        frontier = set([node])

        while frontier:
            next_frontier = set()
            for fnode in frontier:
                found.add(fnode)

                for fnbr in graph[fnode]:
                    if fnbr not in found:
                        if key(fnode, fnbr):
                            if fnode < fnbr:
                                found_edges.append(
                                    (fnode, fnbr)
                                )
                            else:
                                found_edges.append(
                                    (fnbr, fnode)
                                )
                            next_frontier.add(fnbr)

            frontier = next_frontier

        found_edges = list(set(found_edges))
        # return nx.Graph(graph).subgraph(found)
        return nx.Graph(found_edges)

    def component(self, node, color):
        """Return the `color`-component in which `node` lies.
        """

        def search_key(fnode, fnbr):
            return self.edge_color(fnode, fnbr) == color

        return self.breadth_first_search(node, search_key)

    def coord(self, vertex1, vertex2):
        return min(
            i
            for i in range(len(vertex1))
            if vertex1[i] != vertex2[i]
        )

    def chain_factorization(self, component):
        """Return a chain factorization of `component`.
        """

        if not component:
            return [tuple()]

        chains = [
            [(min(component), nbr)]
            for nbr in component[min(component)]
        ]

        edge_added = True
        while edge_added:
            edge_added = False
            for chain in chains:
                tip = chain[-1][-1]

                for candidate_vtx in component[tip]:
                    if candidate_vtx > tip:
                        candidate_edge = (
                            tip,
                            candidate_vtx,
                        )
                        matching_edges = set(
                            tuple(edge)
                            for chain2 in chains
                            for edge in chain2
                            if self.coord(*edge)
                            == self.coord(*candidate_edge)
                            and chain2 != chain
                        )
                        if not matching_edges:
                            edge_added = True
                            chain.append(candidate_edge)

        return tuple(tuple(chain) for chain in chains)

    def chain_changes(self, component):
        cf = self.chain_factorization(component)

        changes = [
            set(self.coord(*e) for e in chain)
            for chain in cf
        ]

        return changes

    def faces(self, component):
        if not component:
            return []

        top, bot = min(component), max(component)

        changes = self.chain_changes(component)

        for change in changes:

            def search_key(fnode, fnbr):
                return self.coord(fnode, fnbr) not in change

            yield self.breadth_first_search(
                top, search_key, graph=component
            )
            yield self.breadth_first_search(
                bot, search_key, graph=component
            )

    def vertex_weight(self, node):
        """Return the weight of `node` in this lattice.
        """
        weight = []
        for color in self.colors():
            component = self.component(node, color)
            ranks = [self.rank(node) for node in component]
            top_rank, bottom_rank = min(ranks), max(ranks)

            rank = top_rank - self.rank(node)
            depth = self.rank(node) - bottom_rank

            this_weight = rank - depth
            weight.append(this_weight)

        return tuple(weight)

    def node_label(self, node):
        return "".join(str(c) for c in node)

    def draw(self, eppstein=False, coordinates=None, cmap="Blues"):
        if coordinates is None:
            coordinates = self.coordinates

        if eppstein:
            from eppstein_layout import eppstein_layout

            pos = eppstein_layout(
                self, coordinates=coordinates
            )
        else:
            pos = nx.kamada_kawai_layout(self)

        cmap = plt.cm.get_cmap(cmap, len(self.colors()))

        node_labels = {v: self.node_label(v) for v in self}

        edge_colors = {
            (v1, v2): c
            for v1, v2, c in self.edges(data="color")
        }

        nx.draw_networkx(
            self,
            pos,
            edge_color=edge_colors.values(),
            edge_cmap=cmap,
            vmin=min(self.colors()),
            vmax=max(self.colors()),
            node_size=0,
            labels=node_labels,
            font_size=8,
            font_family="serif",
        )

        sm = plt.cm.ScalarMappable(
            cmap=cmap,
            norm=plt.Normalize(
                vmin=min(self.colors()) - 1,
                vmax=max(self.colors()),
            ),
        )

        cbar = plt.colorbar(
            sm, ticks=[c - 0.5 for c in self.colors()]
        )

        cbar.ax.set_yticklabels(
            [str(x) for x in self.colors()]
        )

        plt.show()
