from dc_lattice import DCLattice


class IncrementalLattice(DCLattice):
    def __init__(self):
        super().__init__()

    def potential_children(self, vertex):
        for idx, value in enumerate(vertex):
            vertexcopy = [x for x in vertex]
            vertexcopy[idx] += 1

            yield tuple(vertexcopy)

    def meet(self, vertex1, vertex2):
        return tuple(max(x) for x in zip(vertex1, vertex2))

    def join(self, vertex1, vertex2):
        return tuple(min(x) for x in zip(vertex1, vertex2))

    def edge_color(self, vertex1, vertex2):
        for idx, value in enumerate(vertex1):
            if abs(vertex1[idx] - vertex2[idx]) == 1:
                return idx + 1
        return None

    def is_adjacent(self, vertex1, vertex2):
        for idx, value in enumerate(vertex1):
            if abs(vertex1[idx] - vertex2[idx]) == 1:
                return True
        return False
