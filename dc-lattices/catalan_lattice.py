#!/usr/bin/env python3

import numpy as np

from dc_lattice import DCLattice
from gt_pattern import gt_patterns, is_gt


class CatalanLattice(DCLattice):
    def __init__(self, size, cap):
        self.size = size
        self.cap = cap

        super().__init__()

    def node_label(self, node):
        return "\n".join(
            "".join(str(i) for i in row) for row in node
        )

    def generate_vertices(self):
        for mat in gt_patterns(self.size, cap=self.cap):
            yield tuple(tuple(row) for row in mat)

    def is_valid_label(self, vertex):
        return is_gt(vertex, cap=self.cap)

    def potential_children(self, vertex):
        for x, row in enumerate(vertex):
            for y, entry in enumerate(row):
                if entry is not None:
                    if x == self.size - 1:
                        delta = 2
                    else:
                        delta = 1
                    yield tuple(
                        tuple(
                            c + delta
                            if (x == x2 and y == y2)
                            else c
                            for y2, c in enumerate(row2)
                        )
                        for x2, row2 in enumerate(vertex)
                    )

    def rank(self, vertex):
        return sum(sum(row) for row in vertex)

    def colors(self):
        return range(self.size)

    def is_adjacent(self, vertex1, vertex2):
        differences = 0
        for x in range(self.size):
            for y in range(x + 1):
                if vertex1[x][y] != vertex2[x][y]:
                    if x == self.size - 1:
                        delta = 2
                    else:
                        delta = 1

                    if (
                        abs(vertex1[x][y] - vertex2[x][y])
                        != delta
                    ):
                        return False
                    else:
                        differences += 1
        return differences == 1

    def edge_color(self, vertex1, vertex2):
        if self.is_adjacent(vertex1, vertex2):
            for x in range(self.size):
                for y in range(x + 1):
                    if vertex1[x][y] != vertex2[x][y]:
                        return x

    def meet(self, vertex1, vertex2):
        return tuple(
            tuple(max(c1, c2) for c1, c2 in zip(row1, row2))
            for row1, row2 in zip(vertex1, vertex2)
        )

    def join(self, vertex1, vertex2):
        return tuple(
            tuple(min(c1, c2) for c1, c2 in zip(row1, row2))
            for row1, row2 in zip(vertex1, vertex2)
        )

    def edge_color(self, vertex1, vertex2):
        for row_idx, row in enumerate(vertex1):
            if row_idx == self.size - 1:
                delta = 2
            else:
                delta = 1

            for col_idx, entry in enumerate(row):
                if (
                    abs(entry - vertex2[row_idx][col_idx])
                    == delta
                ):
                    return row_idx + 1
        return None

    def is_adjacent(self, vertex1, vertex2):
        for row_idx, row in enumerate(vertex1):
            if row_idx == self.size - 1:
                delta = 2
            else:
                delta = 1

            for col_idx, entry in enumerate(row):
                if (
                    abs(entry - vertex2[row_idx][col_idx])
                    == delta
                ):
                    return row_idx + 1
        return None

    def coord(self, vertex1, vertex2):
        if self.is_adjacent(vertex1, vertex2):
            for x in range(self.size):
                for y in range(x + 1):
                    if vertex1[x][y] != vertex2[x][y]:
                        return (x, y)

    def coordinates(self, vertex):
        coords = list(list(row[:(idx + 1)]) for idx, row in enumerate(vertex))
        coords[-1] = list(coord // 2 for coord in coords[-1])
        return np.concatenate(coords)
