#!/usr/bin/env python3

from itertools import product

from incremental_lattice import IncrementalLattice


class ZPSLattice(IncrementalLattice):
    def __init__(self, n):
        self.n = n

        super().__init__()

    def generate_vertices(self):
        for bools in product([True, False], repeat=self.n):
            members = [
                x if b else 0
                for x, b in zip(range(1, self.n + 1), bools)
            ]

            members = tuple(sorted(members))
            yield members

    def colors(self):
        return range(1, self.n + 1)

    def is_valid_label(self, potential_child):
        for idx in range(self.n - 1):
            if potential_child[idx] not in range(
                0, self.n + 1
            ):
                return False
            if (
                0
                < potential_child[idx]
                == potential_child[idx + 1]
            ):
                return False
            if (
                potential_child[idx]
                > potential_child[idx + 1]
            ):
                return False

        if potential_child[-1] not in range(0, self.n + 1):
            return False
        return True

    def edge_color(self, vertex1, vertex2):
        diff_idx = min(
            i
            for i in range(len(vertex1))
            if vertex1[i] != vertex2[i]
        )

        min_coord = min(
            vertex1[diff_idx], vertex2[diff_idx]
        )

        return self.n - min_coord
