\beamer@sectionintoc {1}{Dramatis personae}{2}{0}{1}
\beamer@subsectionintoc {1}{1}{Diamond-colored distributive lattices}{3}{0}{1}
\beamer@subsectionintoc {1}{2}{Semisimple Lie algebras and root systems}{15}{0}{1}
\beamer@subsectionintoc {1}{3}{Representation diagrams}{18}{0}{1}
\beamer@sectionintoc {2}{Some examples}{20}{0}{2}
\beamer@sectionintoc {3}{Main results}{31}{0}{3}
\beamer@sectionintoc {4}{Paths and tableaux}{38}{0}{4}
\beamer@sectionintoc {5}{Code demo}{46}{0}{5}
