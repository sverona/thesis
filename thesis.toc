\contentsline {chapter}{\numberline {1}Introduction}{10}{chapter.1}%
\contentsline {chapter}{\numberline {2}Preliminaries}{12}{chapter.2}%
\contentsline {section}{\numberline {2.1}Some aspects of combinatorial lattice theory}{12}{section.2.1}%
\contentsline {subsection}{\numberline {2.1.1}Partially ordered sets}{12}{subsection.2.1.1}%
\contentsline {subsection}{\numberline {2.1.2}Lattices}{15}{subsection.2.1.2}%
\contentsline {subsection}{\numberline {2.1.3}Lattices from posets}{15}{subsection.2.1.3}%
\contentsline {subsection}{\numberline {2.1.4}Birkhoff's representation theorem}{16}{subsection.2.1.4}%
\contentsline {subsection}{\numberline {2.1.5}Diamond-colorings}{17}{subsection.2.1.5}%
\contentsline {subsection}{\numberline {2.1.6}Monochromatic components}{18}{subsection.2.1.6}%
\contentsline {subsection}{\numberline {2.1.7}Incremental lattices}{20}{subsection.2.1.7}%
\contentsline {section}{\numberline {2.2}Lie theory}{23}{section.2.2}%
\contentsline {subsection}{\numberline {2.2.1}Classical Lie algebras}{25}{subsection.2.2.1}%
\contentsline {subsection}{\numberline {2.2.2}Ideals and simplicity}{29}{subsection.2.2.2}%
\contentsline {subsection}{\numberline {2.2.3}Toral subalgebras and roots}{30}{subsection.2.2.3}%
\contentsline {subsection}{\numberline {2.2.4}Cartan matrices}{39}{subsection.2.2.4}%
\contentsline {subsection}{\numberline {2.2.5}Serre relations and Chevalley generators}{40}{subsection.2.2.5}%
\contentsline {subsection}{\numberline {2.2.6}Some general representation theory}{41}{subsection.2.2.6}%
\contentsline {subsection}{\numberline {2.2.7}Combinatorial structure of semisimple Lie algebra representations*}{45}{subsection.2.2.7}%
\contentsline {chapter}{\numberline {3}Lattice models of Lie algebra representations}{48}{chapter.3}%
\contentsline {section}{\numberline {3.1}The DCS relations}{48}{section.3.1}%
\contentsline {section}{\numberline {3.2}Example: chains}{50}{section.3.2}%
\contentsline {section}{\numberline {3.3}Example: Boolean lattices}{51}{section.3.3}%
\contentsline {section}{\numberline {3.4}Example: zero-padded subset lattices}{52}{section.3.4}%
\contentsline {section}{\numberline {3.5}Example: Fibonaccian lattices}{55}{section.3.5}%
\contentsline {subsection}{\numberline {3.5.1}Solitarity and edge-minimality of \(\mathscr F(3, 3)\)}{57}{subsection.3.5.1}%
\contentsline {chapter}{\numberline {4}Catalanian lattices}{61}{chapter.4}%
\contentsline {section}{\numberline {4.1}Gelfand-Tsetlin patterns}{61}{section.4.1}%
\contentsline {section}{\numberline {4.2}The cardinality of \(L_C^{Cat}(n, k)\)}{62}{section.4.2}%
\contentsline {section}{\numberline {4.3}Uniqueness of DCS coefficients}{74}{section.4.3}%
\contentsline {chapter}{\numberline {5}Paths and tableaux}{77}{chapter.5}%
\contentsline {section}{\numberline {5.1}Motzkin paths}{77}{section.5.1}%
\contentsline {section}{\numberline {5.2}Littlewood-Richardson tableaux}{79}{section.5.2}%
\contentsline {section}{\numberline {5.3}Equinumeracy?}{80}{section.5.3}%
\contentsline {chapter}{\numberline {6}Computational methods}{82}{chapter.6}%
\contentsline {section}{\numberline {6.1}Drawing DC lattices}{82}{section.6.1}%
\contentsline {subsection}{\numberline {6.1.1}Eppstein's graph drawing algorithm}{82}{subsection.6.1.1}%
\contentsline {section}{\numberline {6.2}Certifying solitarity in DC lattices}{85}{section.6.2}%
\contentsline {chapter}{Appendices}{87}{section*.27}%
\contentsline {chapter}{\numberline {A}The \texttt {dc-lattices} library}{88}{appendix.1.A}%
\contentsline {chapter}{\numberline {B}Proof of the DCS theorem}{108}{appendix.1.B}%
\contentsline {chapter}{\numberline {C}DCS coefficients for $L_A^{Fib}(4, 3)$ and $L_A^{Fib}(5, 3)$}{113}{appendix.1.C}%
