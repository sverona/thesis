\documentclass{beamer}

\usepackage[utf8]{inputenc}
\usepackage{mathtools,mathrsfs}
\usepackage{graphics}
\usepackage{ytableau}

\usepackage{float}
\usepackage{subfig}
\usepackage{pstricks,pstricks-add}

\title{Some Generalizations of Classical Integer Sequences Arising in Combinatorial Representation Theory}
\author{Sasha Verona Malone}
\institute{WKU}
\date{November 9, 2020}
\usetheme{Madrid}
\usecolortheme{seahorse}
\setbeamercolor{enumerate item}{fg=black}
\setbeamercolor{enumerate subitem}{fg=black}
\setbeamercolor{item projected}{fg=black}

\theoremstyle{definition}
\newtheorem{proposition}{Proposition}
\newtheorem{conjecture}{Conjecture}
\providecommand{\tightlist}{%
\setlength{\itemsep}{0pt}\setlength{\parskip}{0pt}}

\begin{document}
\frame{\titlepage}

\AtBeginSection[]
{
\begin{frame}
\frametitle{Table of Contents}
\tableofcontents[currentsection]
\end{frame}
}

\section{Dramatis personae}
\subsection{Diamond-colored distributive lattices}
\begin{frame}
  \frametitle{Lattices}
  \begin{definition}
  A \textit{poset} is a set equipped with a partial order.
  \end{definition}
  \pause

  \begin{definition}
  A \textit{lattice} is a poset in which any two elements $x, y$ have
  \begin{enumerate}
    \item a least upper bound (called the \textit{join} $x \vee y$) and
    \item a greatest lower bound (called the \textit{meet} $x \wedge y$).
  \end{enumerate}
  \end{definition}

  \begin{definition}
  The \textit{Hasse diagram} of a lattice $L$ is the directed graph with vertex set $L$ in which there is a directed edge $x \to y$ if and only if $y$ covers $x$.
  \end{definition}

  We will view lattices as Hasse diagrams in the sequel --- we'll refer to elements as \textit{vertices} and covering relations as \textit{edges.}
\end{frame}

\begin{frame}
  \begin{figure}
    \centering
    \scalebox{0.7}{%
\subfloat[The Boolean lattice \(\mathscr B_3\).]{%
\begin{pspicture}(-3,-3)(3,3)
\psdot*(0,1.5)\uput*[0](0,1.5){\footnotesize \{1,2,3\}}
\psdot*(-1,0.5)\uput*[180](-1,0.5){\footnotesize \{1,2\}}
     \pcline{->}(-1,0.5)(0,1.5)
\psdot*(0,0.5)\uput*[0](0,0.5){\footnotesize \{1,3\}}
     \pcline{->}(0,0.5)(0,1.5)
\psdot*(1,0.5)\uput*[0](1,0.5){\footnotesize \{2,3\}}
     \pcline{->}(1,0.5)(0,1.5)
\psdot*(-1,-0.5)\uput*[180](-1,-0.5){\footnotesize \{1\}}
     \pcline{->}(-1,-0.5)(-1,0.5)
     \pcline{->}(-1,-0.5)(0,0.5)
\psdot*(0,-0.5)\uput*[0](0,-0.5){\footnotesize \{2\}}
     \pcline{->}(0,-0.5)(-1,0.5)
     \pcline{->}(0,-0.5)(1,0.5)
\psdot*(1,-0.5)\uput*[0](1,-0.5){\footnotesize \{3\}}
     \pcline{->}(1,-0.5)(0,0.5)
     \pcline{->}(1,-0.5)(1,0.5)
\psdot*(0,-1.5)\uput*[0](0,-1.5){\footnotesize $\varnothing$}
     \pcline{->}(0,-1.5)(-1,-0.5)
     \pcline{->}(0,-1.5)(0,-0.5)
     \pcline{->}(0,-1.5)(1,-0.5)
\end{pspicture}
}\hfill
\subfloat[The antichain \(j(\mathscr B_3)\).]{%
\begin{pspicture}(-3,-2)(3, 2)
\psdot*(-2,0)\uput*[0](-2,0){\footnotesize 1}
\psdot*(0,0)\uput*[0](0,0){\footnotesize 2}
\psdot*(2,0)\uput*[0](2,0){\footnotesize 3}
\end{pspicture}
}
    }
    \end{figure}
\end{frame}

\begin{frame}
\begin{figure}
  \centering
\scalebox{0.7}{%
\subfloat[The zero-padded subset lattice $\mathscr Z_4.$]{%
\begin{pspicture}(-3, -6)(3, 5)
\psdot*(0,5)\uput*[0](0,5){\footnotesize 1234}
    \pcline{->}(0,4)(0,5)
\psdot*(0,4)\uput*[0](0,4){\footnotesize 0234}
    \pcline{->}(0,3)(0,4)
\psdot*(0,3)\uput*[0](0,3){\footnotesize 0134}
\psdot*(-1,2)\uput*[0](-1,2){\footnotesize 0034}
    \pcline{->}(-1,2)(0,3)
    \pcline{->}(1,2)(0,3)
\psdot*(1,2)\uput*[0](1,2){\footnotesize 0124}
\psdot*(0,1)\uput*[0](0,1){\footnotesize 0024}
    \pcline{->}(0,1)(-1,2)
    \pcline{->}(0,1)(1,2)
\psdot*(2,1)\uput*[0](2,1){\footnotesize 0123}
    \pcline{->}(2,1)(1,2)
\psdot*(-1,0)\uput*[0](-1,0){\footnotesize 0014}
    \pcline{->}(-1,0)(0,1)
\psdot*(1,0)\uput*[0](1,0){\footnotesize 0023}
    \pcline{->}(1,0)(0,1)
\psdot*(-2,-1)\uput*[0](-2,-1){\footnotesize 0004}
    \pcline{->}(1,0)(2,1)
    \pcline{->}(-2,-1)(-1,0)
\psdot*(0,-1)\uput*[0](0,-1){\footnotesize 0013}
    \pcline{->}(0,-1)(-1,0)
    \pcline{->}(0,-1)(1,0)
\psdot*(-1,-2)\uput*[0](-1,-2){\footnotesize 0003}
    \pcline{->}(-1,-2)(-2,-1)
    \pcline{->}(-1,-2)(0,-1)
\psdot*(1,-2)\uput*[0](1,-2){\footnotesize 0012}
    \pcline{->}(1,-2)(0,-1)
\psdot*(0,-3)\uput*[0](0,-3){\footnotesize 0002}
     \pcline{->}(0,-3)(-1, -2)
     \pcline{->}(0,-3)(1,-2)
\psdot*(0,-4)\uput*[0](0,-4){\footnotesize 0001}
     \pcline{->}(0,-4)(0,-3)
\psdot*(0,-5)\uput*[0](0,-5){\footnotesize 0000}
     \pcline{->}(0,-5)(0,-4)
\end{pspicture}
}\hfill
\subfloat[The ``angelfish'' poset $j(\mathscr Z_4).$]{%
\begin{pspicture}(-1,-1)(4,6)
\psdot*(0,0)\uput*[0](0,0){\footnotesize $(1, 1)$}
\psdot*(1,1)\uput*[0](1,1){\footnotesize $(1, 2)$}
\psdot*(0,2)\uput*[0](0,2){\footnotesize $(2, 2)$}
\psdot*(2,2)\uput*[0](2,2){\footnotesize $(1, 3)$}
\psdot*(1,3)\uput*[0](1,3){\footnotesize $(2, 3)$}
\psdot*(3,3)\uput*[0](3,3){\footnotesize $(1, 4)$}
\psdot*(0,4)\uput*[0](0,4){\footnotesize $(3, 3)$}
\psdot*(2,4)\uput*[0](2,4){\footnotesize $(2, 4)$}
\psdot*(1,5)\uput*[0](1,5){\footnotesize $(3, 4)$}
\psdot*(0,6)\uput*[0](0,6){\footnotesize $(4, 4)$}
\pcline{->}(0,0)(1,1)
\pcline{->}(1,1)(0,2)
\pcline{->}(1,1)(2,2)
\pcline{->}(0,2)(1,3)
\pcline{->}(2,2)(1,3)
\pcline{->}(2,2)(3,3)
\pcline{->}(1,3)(0,4)
\pcline{->}(1,3)(2,4)
\pcline{->}(3,3)(2,4)
\pcline{->}(0,4)(1,5)
\pcline{->}(2,4)(1,5)
\pcline{->}(1,5)(0,6)
\end{pspicture}
}
}
\end{figure}
\end{frame}

\begin{frame}
  \frametitle{Distributive lattices}
  \begin{itemize}
  \item If the elements of a lattice are sets, the join and meet become the union and intersection respectively.\pause
  \item Unions and intersections distribute over each other, viz. $$A \cup (B \cap C) = (A \cup B) \cap (A \cap C).$$
\end{itemize}
  \pause

  \begin{definition}
  A \textit{distributive} lattice is a lattice in which the join and meet distribute over each other, viz. $$x \vee (y \wedge z) = (x \vee y) \wedge (x \vee z).$$
  \end{definition}
\end{frame}

\begin{frame}
  \frametitle{Lattices from posets}
  It turns out that there is a 1-1 correspondence between finite posets and finite distributive lattices.

  \begin{block}{Fundamental Theorem of Finite Distributive Lattices (Birkhoff, 1937)}
    Any finite distributive lattice is representable as a lattice of sets.
  \end{block}

  The sets in this representation are the downward-closed sets in the poset of the lattice's \textit{join-irreducible} elements; an element in a (finite) lattice is join-irreducible iff it covers exactly one element.
\end{frame}


\begin{frame}
  \frametitle{Diamond-colored distributive lattices}

  \begin{itemize}
  \item If we color the vertices of a poset before turning it into a distributive lattice, the colors naturally lift to the edges.\pause
  \item We say that lattices whose edges are colored in this way are \textit{diamond-colored distributive lattices.}
  \end{itemize}
  \begin{figure}
    \centering
    \scalebox{0.7}{%
\subfloat[The Boolean lattice \(\mathscr B_3\), edge-colored.]{%
\begin{pspicture}(-3,-3)(3,3)
\psdot*(0,1.5)\uput*[0](0,1.5){\footnotesize \{1,2,3\}}
\psdot*(-1,0.5)\uput*[180](-1,0.5){\footnotesize \{1,2\}}
     \pcline[linecolor=green]{->}(-1,0.5)(0,1.5)
\psdot*(0,0.5)\uput*[0](0,0.5){\footnotesize \{1,3\}}
     \pcline[linecolor=blue]{->}(0,0.5)(0,1.5)
\psdot*(1,0.5)\uput*[0](1,0.5){\footnotesize \{2,3\}}
     \pcline[linecolor=red]{->}(1,0.5)(0,1.5)
\psdot*(-1,-0.5)\uput*[180](-1,-0.5){\footnotesize \{1\}}
     \pcline[linecolor=blue]{->}(-1,-0.5)(-1,0.5)
     \pcline[linecolor=green]{->}(-1,-0.5)(0,0.5)
\psdot*(0,-0.5)\uput*[0](0,-0.5){\footnotesize \{2\}}
     \pcline[linecolor=red]{->}(0,-0.5)(-1,0.5)
     \pcline[linecolor=green]{->}(0,-0.5)(1,0.5)
\psdot*(1,-0.5)\uput*[0](1,-0.5){\footnotesize \{3\}}
     \pcline[linecolor=red]{->}(1,-0.5)(0,0.5)
     \pcline[linecolor=blue]{->}(1,-0.5)(1,0.5)
\psdot*(0,-1.5)\uput*[0](0,-1.5){\footnotesize $\varnothing$}
     \pcline[linecolor=red]{->}(0,-1.5)(-1,-0.5)
     \pcline[linecolor=blue]{->}(0,-1.5)(0,-0.5)
     \pcline[linecolor=green]{->}(0,-1.5)(1,-0.5)
\end{pspicture}
}\hfill
\subfloat[The antichain \(j(\mathscr B_3)\), vertex-colored.]{%
\begin{pspicture}(-3,-2)(3, 2)
\psdot*[linecolor=red](-2,0)\uput*[0](-2,0){\footnotesize 1}
\psdot*[linecolor=blue](0,0)\uput*[0](0,0){\footnotesize 2}
\psdot*[linecolor=green](2,0)\uput*[0](2,0){\footnotesize 3}
\end{pspicture}
}
    }
    \end{figure}
\end{frame}

\begin{frame}
  \frametitle{Components and weights}

\begin{definition}
Let $L$ be a DCDL whose edges are colored by $[n]$, and let $k \in [n].$ A $k$-\textit{monochromatic component} of $L$ or just $k$-\textit{component} is a subset $C \subset L$ such that
\begin{enumerate}
\tightlist
\item $C$ is a subposet of $L$ under the induced order;
\item if $x \to y$ in $C$, then $x \to y$ in $L$;
\item and $C$ is maximal with respect to the property that all edges have color $k$.
\end{enumerate}
\end{definition}
\end{frame}

\begin{frame}
  \frametitle{Components and weights}

  \begin{definition}
    Let $L$ be a DCDL. For any vertex $v \in L$ belonging to a $k$-component, the \textit{color $k$ weight of $v$} $\omega_k(v)$ is given by the rank of $v$ in its $k$-component minus the depth of $v$ in its $k$-component.
  \end{definition}

\begin{figure}
\centering
\begin{pspicture}(-2,-2)(2,2)
\psdot*(0,-1.5)
\uput*[270](0,-1.5){$w$, weight -2}
\psdot*(-1.5, 0)
\uput*[180](-1.5,0){$x$, weight 0}
\psdot*(1.5,0)
\uput*[0](1.5,0){$y$, weight 0}
\psdot*(0,1.5)
\uput*[90](0,1.5){$z$, weight 2}
\psline[linewidth=1pt,linecolor=black,arrows=->](0,-1.5)(-1.5,0)
\psline[linewidth=1pt,linecolor=black,arrows=->](0,-1.5)(1.5,0)
\psline[linewidth=1pt,linecolor=black,arrows=->](-1.5,0)(0,1.5)
\psline[linewidth=1pt,linecolor=black,arrows=->](1.5,0)(0,1.5)
\end{pspicture}
\end{figure}
\end{frame}

\subsection{Semisimple Lie algebras and root systems}
\begin{frame}
  \frametitle{Semisimple Lie algebras}
\begin{definition}
A \textit{Lie algebra} is a vector space $\mathfrak g$ together with a \textit{bracket} operation $[ \cdot, \cdot ] : \mathfrak g \times \mathfrak g \to \mathfrak g$ satisfying the following axioms for each $x, y, z \in \mathfrak g$ and scalars $a, b$:
\begin{enumerate}
\tightlist\item\textit{bilinearity:} $[ax + by, z] = a[x, z] + b[y, z]$;
\item \textit{anticommutativity:} $[x, y] = -[y, x]$;
\item \textit{Jacobi identity:} $[x, [y, z]] + [y, [z, x]] + [z, [x, y]] = 0.$
\end{enumerate}
\end{definition}

A Lie algebra is \textit{semisimple} if it satisfies some additional structure conditions. It will suffice for us to think of semisimple Lie algebras as being the well-behaved ones.
\end{frame}

\begin{frame}
  \frametitle{Root systems}

\begin{definition}
Let $\mathbb R^n$ be a vector space with the usual inner product. Then a finite spanning set of vectors $\Phi \subset \mathbb R^n$ is said to be a \textit{root system} if
\begin{enumerate}
\tightlist
\item $\mathbf 0 \not \in \Phi$.
\item For any $\alpha \in \Phi$, the only other scalar multiple of $\alpha$ belonging to $\Phi$ is $-\alpha$.
\item For any $\alpha \in \Phi$, $\Phi$ is symmetric about $\alpha$.
\item For any $\alpha, \beta \in \Phi$, $\alpha$ is separated from its reflection in the hyperplane perpendicular to $\beta$ by an integer multiple of $\beta$ $\langle \alpha, \beta \rangle$.
\end{enumerate}
\end{definition}

\begin{figure}[H]
\centering
\scalebox{0.7}{%
\subfloat[$A_1$, rank 1]{%
\begin{pspicture}(-2, -2)(2, 2)
\psdot*(0, 0)
\psline[linewidth=1pt, linecolor=black,arrows=->](0, 0)(1.5,0)
\psline[linewidth=1pt, linecolor=black,arrows=->](0, 0)(-1.5,0)
\uput*[0](1.5,0){$\alpha$}
\end{pspicture}
}
\subfloat[$A_2$, rank 2]{%
\begin{pspicture}(-2, -2)(2, 2)
\psdot*(0, 0)
\psline[linewidth=1pt, linecolor=black,arrows=->](0, 0)(1.5,0)
\psline[linewidth=1pt, linecolor=black,arrows=->](0, 0)(-1.5,0)
\psline[linewidth=1pt, linecolor=black,arrows=->](0, 0)(0.75,1.3)
\psline[linewidth=1pt, linecolor=black,arrows=->](0, 0)(-0.75,1.3)
\psline[linewidth=1pt, linecolor=black,arrows=->](0, 0)(0.75,-1.3)
\psline[linewidth=1pt, linecolor=black,arrows=->](0, 0)(-0.75,-1.3)
\uput*[0](1.5,0){$\alpha$}
\uput*[120](-0.75,1.3){$\beta$}
\end{pspicture}
}
\subfloat[$B_2$, rank 2]{%
\begin{pspicture}(-2, -2)(2, 2)
\psdot*(0, 0)
\psline[linewidth=1pt, linecolor=black,arrows=->](0, 0)(1.5,0)
\psline[linewidth=1pt, linecolor=black,arrows=->](0, 0)(-1.5,0)
\psline[linewidth=1pt, linecolor=black,arrows=->](0, 0)(0, 1.5)
\psline[linewidth=1pt, linecolor=black,arrows=->](0, 0)(0, -1.5)
\psline[linewidth=1pt, linecolor=black,arrows=->](0, 0)(1.5, 1.5)
\psline[linewidth=1pt, linecolor=black,arrows=->](0, 0)(-1.5, -1.5)
\psline[linewidth=1pt, linecolor=black,arrows=->](0, 0)(1.5, -1.5)
\psline[linewidth=1pt, linecolor=black,arrows=->](0, 0)(-1.5, 1.5)
\uput*[0](1.5,0){$\alpha$}
\uput*[135](-1.5,1.5){$\beta$}
\end{pspicture}
}
\subfloat[$G_2$, rank 2]{%
\begin{pspicture}(-2, -2)(2, 2)
\psdot*(0, 0)
\psline[linewidth=1pt, linecolor=black,arrows=->](0, 0)(0.865,0)
\psline[linewidth=1pt, linecolor=black,arrows=->](0, 0)(-0.865,0)
\psline[linewidth=1pt, linecolor=black,arrows=->](0, 0)(0.4327,0.75)
\psline[linewidth=1pt, linecolor=black,arrows=->](0, 0)(-0.4327,0.75)
\psline[linewidth=1pt, linecolor=black,arrows=->](0, 0)(0.4327,-0.75)
\psline[linewidth=1pt, linecolor=black,arrows=->](0, 0)(-0.4327,-0.75)
\psline[linewidth=1pt, linecolor=black,arrows=->](0, 0)(0, 1.5)
\psline[linewidth=1pt, linecolor=black,arrows=->](0, 0)(0, -1.5)
\psline[linewidth=1pt, linecolor=black,arrows=->](0, 0)(1.3, 0.75)
\psline[linewidth=1pt, linecolor=black,arrows=->](0, 0)(1.3, -0.75)
\psline[linewidth=1pt, linecolor=black,arrows=->](0, 0)(-1.3, 0.75)
\psline[linewidth=1pt, linecolor=black,arrows=->](0, 0)(-1.3, -0.75)
\uput*[0](0.865,0){$\alpha$}
\uput*[150](-1.3,0.75){$\beta$}
\end{pspicture}
}
}
\end{figure}
\end{frame}

\begin{frame}
  \frametitle{Serre relations}
  \begin{theorem}
    There is a 1-1 correspondence between root systems and semisimple Lie algebras.

    More precisely, let $\Phi$ be a rank-$n$ root system with base $\Delta = \{\phi_i\}_{i=1}^n$. Then the Lie algebra with generators $\{x_i, y_i, h_i\}_{i=1}^n$ subject to the relations
    \begin{enumerate}
    \tightlist
    \item $[h_i, h_j] = 0$;
    \item $[x_i, y_j] = \mathbf 1_{i=j} h_i$;
    \item $[h_i, x_j] = \langle \phi_j, \phi_i\rangle x_j$;
    \item $[h_i, y_j] = -\langle \phi_j, \phi_i\rangle y_j$;
    \item $(\operatorname{ad} x_i)^{1-\langle \phi_j, \phi_i \rangle}(x_j)  = (\operatorname{ad} y_i)^{1-\langle \phi_j, \phi_i \rangle}(y_j) =0$
    \end{enumerate}
    is semisimple and has root system $\Phi$.
  \end{theorem}
\end{frame}

\subsection{Representation diagrams}
\begin{frame}
  \frametitle{The diamond, crossing, and structure relations}
\begin{definition}
Let $L$ be a DCDL and $\Phi$ a root system. If for any edge $x \to y$ there exist $c_{yx}, d_{xy} \in \mathbb{C}$ such that
\begin{enumerate}
\tightlist
\item whenever $w, x, y, z \in L$ form a diamond with $w \to x \to z$, $w \to y \to z$, we have the \textit{diamond relations} $c_{yw}d_{wx} = d_{xz}c_{zy}, c_{xw}d_{wy} = d_{yz}c_{zx},$
\item and for any $x \in L$ and any $i \in [n]$, we have the \textit{crossing relation}
\begin{align*}
\sum_{\substack{w \in L \\ w \xrightarrow{i} x}} c_{xw}d_{wx} - \sum_{\substack{z \in L \\ x \xrightarrow{i} z}} c_{zx}d_{xz} = \omega_i(x),
\end{align*}
\item and if $x \overset{i}\to y$, then for any $j \in [n]$ we have the \textit{structure relation} $\omega_j(y) -\omega_j(x) = \langle \phi_j, \phi_i \rangle$,
\end{enumerate}
then $L$ is said to obey the \textit{diamond, crossing, and structure relations} (or just the \textit{DCS relations}) of $\Phi$.
\end{definition}
\end{frame}
\begin{frame}
  \frametitle{The DCS theorem}
  The DCS relations turn out to encode the Serre relations on a lattice!
\begin{theorem}[Donnelly]
If $L$ obeys the DCS relations of $\Phi$, then the Lie algebra $\mathfrak g$ which corresponds to $\Phi$ is homomorphic to the Lie algebra $\mathfrak l$ generated by the $3n$ $|L|$-dimensional matrices $\{E_i, F_i, H_i\}_{i=1}^n$ (whose rows and columns are indexed by the elements of $L$) defined as follows:
\begin{enumerate}
\tightlist
\item $[E_i]_{yx} \coloneqq c_{yx}$ if $x \xrightarrow{i} y$, or $0$ otherwise;
\item $[F_i]_{xy} \coloneqq d_{xy}$ if $x \xrightarrow{i} y$, or $0$ otherwise;
\item $H_i \coloneqq [E_iF_i].$
\end{enumerate}
We say that $L$ \textit{realizes a representation of} $\mathfrak g$. Additionally, if $\mathfrak g$ is simple, the representation so realized is faithful.
\end{theorem}
\end{frame}


\section{Some examples}
\begin{frame}
  \frametitle{Boolean lattices $\mathscr B_n$}

\begin{definition}
The \textit{Boolean lattice} $\mathscr B_n$ is defined as follows.
\begin{enumerate}
\tightlist
\item Vertices of $\mathscr B_n$ are subsets $x \subseteq [n].$
\item $\mathscr B_n$ is ordered by containment; if $x, y \in \mathscr B_n,$ $x \leq y$ if $x \subseteq y.$
\item If $x, y \in \mathscr B_n,$ $\rho(x) = |x|$, and consequently $x \to y$ if and only if $|y \setminus x| = 1.$
\item Edges in $\mathscr B_n$ are colored by $[n]$; the edge $x \to y$ has color given by the element of the singleton set $y \setminus x.$
\end{enumerate}
\end{definition}
\begin{proposition}
The Boolean lattice $\mathscr B_n$ realizes a representation of the Lie algebra $A_1^n = \bigoplus_{i=1}^n A_1$.
\end{proposition}
\end{frame}

\begin{frame}
  \begin{figure}
    \centering
    \scalebox{0.7}{%
\subfloat[The Boolean lattice \(\mathscr B_3\).]{%
\begin{pspicture}(-3,-3)(3,3)
\psdot*(0,1.5)\uput*[0](0,1.5){\footnotesize \{1,2,3\}}
\psdot*(-1,0.5)\uput*[180](-1,0.5){\footnotesize \{1,2\}}
     \pcline{->}(-1,0.5)(0,1.5)
\psdot*(0,0.5)\uput*[0](0,0.5){\footnotesize \{1,3\}}
     \pcline{->}(0,0.5)(0,1.5)
\psdot*(1,0.5)\uput*[0](1,0.5){\footnotesize \{2,3\}}
     \pcline{->}(1,0.5)(0,1.5)
\psdot*(-1,-0.5)\uput*[180](-1,-0.5){\footnotesize \{1\}}
     \pcline{->}(-1,-0.5)(-1,0.5)
     \pcline{->}(-1,-0.5)(0,0.5)
\psdot*(0,-0.5)\uput*[0](0,-0.5){\footnotesize \{2\}}
     \pcline{->}(0,-0.5)(-1,0.5)
     \pcline{->}(0,-0.5)(1,0.5)
\psdot*(1,-0.5)\uput*[0](1,-0.5){\footnotesize \{3\}}
     \pcline{->}(1,-0.5)(0,0.5)
     \pcline{->}(1,-0.5)(1,0.5)
\psdot*(0,-1.5)\uput*[0](0,-1.5){\footnotesize $\varnothing$}
     \pcline{->}(0,-1.5)(-1,-0.5)
     \pcline{->}(0,-1.5)(0,-0.5)
     \pcline{->}(0,-1.5)(1,-0.5)
\end{pspicture}
}\hfill
\subfloat[The antichain \(j(\mathscr B_3)\).]{%
\begin{pspicture}(-3,-2)(3, 2)
\psdot*(-2,0)\uput*[0](-2,0){\footnotesize 1}
\psdot*(0,0)\uput*[0](0,0){\footnotesize 2}
\psdot*(2,0)\uput*[0](2,0){\footnotesize 3}
\end{pspicture}
}
    }
    \end{figure}
\end{frame}


\begin{frame}
  \frametitle{Zero-padded subset lattices $\mathscr Z_n$}
\begin{definition}
The \textit{zero-padded subset lattice} \(\mathscr Z_n\) (hereinafter \textit{ZPS lattice}) is defined as follows.
\begin{enumerate}
\tightlist
\item Vertices of $\mathscr Z_n$ are subsets $x \subseteq [n].$
\item If $x, y \in \mathscr Z_n$, enumerate the elements of (for instance) $x$ by $x_1 \geq \dots \geq x_n$, letting $x_j = 0$ if $j > \vert x \vert$. Then $x \leq y$ if $x_i \leq y_i$ for all $i \in [n]$.
\item If $x, y \in \mathscr Z_n$, $\rho(x) = \sum_{i=1}^n x_i$; we have $x \to y$ if and only if $x$, $y$ differ in exactly one index $i \in [n]$, with $x_i + 1 = y_i.$
\item Edges in $\mathscr Z_n$ are colored by $[n].$ Keeping the above setup, the edge $x \to y$ has color $n - x_i.$
\end{enumerate}
\end{definition}
\begin{proposition}
The ZPS lattice $\mathscr Z_n$ realizes a representation of the Lie algebra $B_n$.
\end{proposition}
\end{frame}

\begin{frame}
\begin{figure}
  \centering
\scalebox{0.7}{%
\subfloat[The zero-padded subset lattice $\mathscr Z_4.$]{%
\begin{pspicture}(-3, -6)(3, 5)
\psdot*(0,5)\uput*[0](0,5){\footnotesize 1234}
    \pcline{->}(0,4)(0,5)
\psdot*(0,4)\uput*[0](0,4){\footnotesize 0234}
    \pcline{->}(0,3)(0,4)
\psdot*(0,3)\uput*[0](0,3){\footnotesize 0134}
\psdot*(-1,2)\uput*[0](-1,2){\footnotesize 0034}
    \pcline{->}(-1,2)(0,3)
    \pcline{->}(1,2)(0,3)
\psdot*(1,2)\uput*[0](1,2){\footnotesize 0124}
\psdot*(0,1)\uput*[0](0,1){\footnotesize 0024}
    \pcline{->}(0,1)(-1,2)
    \pcline{->}(0,1)(1,2)
\psdot*(2,1)\uput*[0](2,1){\footnotesize 0123}
    \pcline{->}(2,1)(1,2)
\psdot*(-1,0)\uput*[0](-1,0){\footnotesize 0014}
    \pcline{->}(-1,0)(0,1)
\psdot*(1,0)\uput*[0](1,0){\footnotesize 0023}
    \pcline{->}(1,0)(0,1)
\psdot*(-2,-1)\uput*[0](-2,-1){\footnotesize 0004}
    \pcline{->}(1,0)(2,1)
    \pcline{->}(-2,-1)(-1,0)
\psdot*(0,-1)\uput*[0](0,-1){\footnotesize 0013}
    \pcline{->}(0,-1)(-1,0)
    \pcline{->}(0,-1)(1,0)
\psdot*(-1,-2)\uput*[0](-1,-2){\footnotesize 0003}
    \pcline{->}(-1,-2)(-2,-1)
    \pcline{->}(-1,-2)(0,-1)
\psdot*(1,-2)\uput*[0](1,-2){\footnotesize 0012}
    \pcline{->}(1,-2)(0,-1)
\psdot*(0,-3)\uput*[0](0,-3){\footnotesize 0002}
     \pcline{->}(0,-3)(-1, -2)
     \pcline{->}(0,-3)(1,-2)
\psdot*(0,-4)\uput*[0](0,-4){\footnotesize 0001}
     \pcline{->}(0,-4)(0,-3)
\psdot*(0,-5)\uput*[0](0,-5){\footnotesize 0000}
     \pcline{->}(0,-5)(0,-4)
\end{pspicture}
}\hfill
\subfloat[The ``angelfish'' poset $j(\mathscr Z_4).$]{%
\begin{pspicture}(-1,-1)(4,6)
\psdot*(0,0)\uput*[0](0,0){\footnotesize $(1, 1)$}
\psdot*(1,1)\uput*[0](1,1){\footnotesize $(1, 2)$}
\psdot*(0,2)\uput*[0](0,2){\footnotesize $(2, 2)$}
\psdot*(2,2)\uput*[0](2,2){\footnotesize $(1, 3)$}
\psdot*(1,3)\uput*[0](1,3){\footnotesize $(2, 3)$}
\psdot*(3,3)\uput*[0](3,3){\footnotesize $(1, 4)$}
\psdot*(0,4)\uput*[0](0,4){\footnotesize $(3, 3)$}
\psdot*(2,4)\uput*[0](2,4){\footnotesize $(2, 4)$}
\psdot*(1,5)\uput*[0](1,5){\footnotesize $(3, 4)$}
\psdot*(0,6)\uput*[0](0,6){\footnotesize $(4, 4)$}
\pcline{->}(0,0)(1,1)
\pcline{->}(1,1)(0,2)
\pcline{->}(1,1)(2,2)
\pcline{->}(0,2)(1,3)
\pcline{->}(2,2)(1,3)
\pcline{->}(2,2)(3,3)
\pcline{->}(1,3)(0,4)
\pcline{->}(1,3)(2,4)
\pcline{->}(3,3)(2,4)
\pcline{->}(0,4)(1,5)
\pcline{->}(2,4)(1,5)
\pcline{->}(1,5)(0,6)
\end{pspicture}
}
}
\end{figure}
\end{frame}

\begin{frame}
  \frametitle{Fibonaccian lattices $\mathscr F(n, k)$}

\begin{definition}
The \textit{Fibonaccian lattice} $L_A^{Fib}(n + 1, k) = \mathscr F(n, k)$ is defined as follows.
\begin{enumerate}
\tightlist
\item Vertices of $\mathscr F(n, k)$ are $k$-tuples $x \in \mathbb Z^k$ such that
\begin{enumerate}
\tightlist
\item $x_i \in \{n(i - 1) + 1, \dots, ni\}$ for each $i \in [k]$;
\item if $x_i = ni$, then $x_{i + 1} \not = ni + 1.$\footnote{That is, $x$ contains no consecutive integers.}
\end{enumerate}
\item $\mathscr F(n, k)$ is ordered componentwise: $x \leq y$ if $x_i \leq y_i$ for each $i \in [k].$
\item If $x, y \in \mathscr F(n, k)$, $x \to y$ if and only if $x$ and $y$ differ in exactly one component $i \in [k],$ with $x_i + 1 = y_i.$
\item Edges in $\mathscr F(n, k)$ are colored by $[n - 1]$ depending on the value of $x_i$, using the \textit{snake rule} diagrammed in the next slide.
\end{enumerate}
\end{definition}
\end{frame}

\begin{frame}
\begin{figure}
\centering
\begin{pspicture}(-1,-1)(6,6)
\psbrace[rot=90,nodesepA=-0.2,nodesepB=0.6](-0.5,5)(-0.5,1){\small $n$ rows}
\psbrace[rot=0,nodesepB=0.2,nodesepA=-1](0.5, 0.5)(4.5,0.5){\small$k$ columns}
\psdot*(0.5,1)\uput*[0](0.5,1){$1$}
     \pcline{->}(0.5,1)(0.5,2)\aput{:R}{\tiny$1$}
\psdot*(0.5,2)\uput*[0](0.5,2){$2$}
     \pcline[linestyle=dotted]{-}(0.5,2)(0.5,3)
\psdot*(0.5,3)\uput*[0](0.5,3){$n-2$}
     \pcline{->}(0.5,3)(0.5,4)\aput{:R}{\tiny$n-2$}
\psdot*(0.5,4)\uput*[0](0.5,4){$n-1$}
     \pcline{->}(0.5,4)(0.5,5)\aput{:R}{\tiny$n-1$}
\psdot*(0.5,5)\uput*[45](0.5,5){$n$}
    \pcline[linestyle=dotted]{-}(0.5,5)(2.5,5)
\psdot*(2.5,1)\uput*[315](2.5,1){$2n$}
     \pcline{->}(2.5,2)(2.5,1)\bput{:L}{\tiny$1$}
\psdot*(2.5,2)\uput*[0](2.5,2){$2n - 1$}
     \pcline[linestyle=dotted]{-}(2.5,2)(2.5,3)
\psdot*(2.5,3)\uput*[0](2.5,3){$n+3$}
     \pcline{->}(2.5,4)(2.5,3)\bput{:L}{\tiny$n-2$}
\psdot*(2.5,4)\uput*[0](2.5,4){$n+2$}
     \pcline{->}(2.5,5)(2.5,4)\bput{:L}{\tiny$n-1$}
\psdot*(2.5,5)\uput*[45](2.5,5){$n+1$}
    \pcline[linestyle=dotted]{-}(2.5,1)(4.5, 1)
\end{pspicture}
\caption{The snake rule for $\mathscr F(n, k).$}
\end{figure}
\end{frame}

\begin{frame}
\begin{figure}
\centering
\scalebox{0.5}{%
\begin{pspicture}(-5, -7)(5, 7)
% rank 6
\psdot*(0,6)\uput*[0](0,6){\footnotesize 369}
% rank 5
\psdot*(-2,4)\uput*[0](-2,4){\footnotesize 368}
\psdot*(0,4)\uput*[0](0,4){\footnotesize 359}
\psdot*(2,4)\uput*[0](2,4){\footnotesize 269}
\pcline[linestyle=dashed]{->}(-2,4)(0,6)
\pcline{->}(0,4)(0,6)
\pcline[linestyle=dashed]{->}(2,4)(0,6)
% rank 4
\psdot*(-2,2)\uput*[0](-2,2){\footnotesize 358}
\psdot*(0,2)\uput*[0](0,2){\footnotesize 268}
\psdot*(2,2)\uput*[0](2,2){\footnotesize 259}
\psdot*(4,2)\uput*[0](4,2){\footnotesize 169}
\pcline{->}(-2,2)(-2,4)
\pcline[linestyle=dashed]{->}(-2,2)(0,4)
\pcline[linestyle=dashed]{->}(0,2)(-2,4)
\pcline[linestyle=dashed]{->}(0,2)(2,4)
\pcline[linestyle=dashed]{->}(2,2)(0,4)
\pcline{->}(2,2)(2,4)
\pcline{->}(4,2)(2,4)
% rank 3
\psdot*(-4,0)\uput*[0](-4,0){\footnotesize 357}
\psdot*(0,0)\uput*[0](0,0){\footnotesize 258}
\psdot*(2.2,-0.2)\uput*[0](2.2,-0.2){\footnotesize 249}
\psdot*(1.8,0.2)\uput*[180](1.8,0.2){\footnotesize 168}
\psdot*(4,0)\uput*[0](4,0){\footnotesize 159}
\pcline{->}(-4,0)(-2,2)
\pcline[linestyle=dashed]{->}(0,0)(-2,2)
\pcline{->}(0,0)(0,2)
\pcline[linestyle=dashed]{->}(0,0)(2,2)
\pcline{->}(1.8,0.2)(0,2)
\pcline[linestyle=dashed]{->}(1.8,0.2)(4,2)
\pcline[linestyle=dashed]{->}(2.2,-0.2)(2,2)
\pcline{->}(4,0)(2,2)
\pcline{->}(4,0)(4,2)
% rank 2
\psdot*(-2,-2)\uput*[0](-2,-2){\footnotesize 257}
\psdot*(0,-2)\uput*[0](0,-2){\footnotesize 248}
\psdot*(2,-2)\uput*[0](2,-2){\footnotesize 158}
\psdot*(4,-2)\uput*[0](4,-2){\footnotesize 149}
\pcline[linestyle=dashed]{->}(-2,-2)(-4,0)
\pcline{->}(-2,-2)(0,0)
\pcline[linestyle=dashed]{->}(0,-2)(0,0)
\pcline[linestyle=dashed]{->}(0,-2)(2.2,-0.2)
\pcline{->}(4,-2)(2.2,-0.2)
\pcline{->}(2,-2)(0,0)
\pcline{->}(2,-2)(1.8,0.2)
\pcline[linestyle=dashed]{->}(2,-2)(4,0)
\pcline[linestyle=dashed]{->}(4,-2)(4,0)
% rank 1
\psdot*(-2,-4)\uput*[0](-2,-4){\footnotesize 247}
\psdot*(0,-4)\uput*[0](0,-4){\footnotesize 157}
\psdot*(2,-4)\uput*[0](2,-4){\footnotesize 148}
\pcline[linestyle=dashed]{->}(-2,-4)(-2,-2)
\pcline{->}(-2,-4)(0,-2)
\pcline{->}(0,-4)(-2,-2)
\pcline{->}(0,-4)(2,-2)
\pcline{->}(2,-4)(0,-2)
\pcline[linestyle=dashed]{->}(2,-4)(2,-2)
\pcline[linestyle=dashed]{->}(2,-4)(4,-2)
% rank 0
\psdot*(0,-6)\uput*[0](0,-6){\footnotesize 147}
\pcline{->}(0,-6)(-2,-4)
\pcline[linestyle=dashed]{->}(0,-6)(0,-4)
\pcline{->}(0,-6)(2,-4)
\end{pspicture}
}
\caption{The Fibonaccian lattice $\mathscr F(3,3)$. Solid edges are color 1; dashed edges are color 2.}
\end{figure}
\end{frame}

\begin{frame}
  \frametitle{Catalanian lattices $\mathscr C(n, k)$}

\begin{definition}
A \textit{Gelfand-Tsetlin pattern} or just \textit{GT pattern} of order $n$ is an $n \times n$ lower triangular matrix with integer entries that satisfies $g_{i + 1, j} \leq g_{ij} \leq g_{i +1, j + 1}$ for any $1 \leq j \leq i \leq n.$
\end{definition}

Visually, this says that entries in a GT pattern weakly decrease when
moving northwest or down.

\begin{definition}
  A GT pattern of order $n$ is \textit{symplectic} if $g_{nj}$ is even for any $j$.
\end{definition}
\end{frame}

\begin{frame}
  \frametitle{Catalanian lattices $\mathscr C(n, k)$}
\begin{definition}
  The \textit{Catalanian lattice of order \(n, k\)} $L_C^{Cat}(n, k) = \mathscr C(n, k)$ is defined as follows.
\begin{enumerate}
\tightlist
\item Vertices of $\mathscr C(n, k)$ are symplectic GT patterns of order $n$ and maximum entry $2k.$
\item $\mathscr C(n, k)$ is ordered componentwise.
\item If $x, y \in \mathscr C(n, k)$, $x \to y$ if and only if $x$ and $y$ differ in exactly one entry $(i, j) \in [n]^2$, with $x_{ij} + 1 = y_{ij}$ if $i < n$ or $x_{ij} + 2 = y_{ij}$ if $i = n.$
\item Edges of $\mathscr C(n, k)$ are colored by $[n]$ according to the value of $i$.
\end{enumerate}
\end{definition}
\end{frame}

\begin{frame}
\begin{figure}[p]
\includegraphics[width=0.7\textwidth]{cat32.png}
\caption{The Catalanian lattice \(\mathscr C(3, 1).\)}
\end{figure}
\end{frame}

\section{Main results}
\begin{frame}
  \frametitle{Cardinality of Fibonaccian lattices}
\begin{proposition}
The vertex cardinality of $\mathscr F(n, k)$ is $(r_1^{n+1} - r_2^{n + 1})/(r_1 - r_2)$, where $r_1$ and $r_2$ are respectively the positive and negative roots of $x^2 - kx - 1$.
\end{proposition}
\pause
\begin{proof}
Observe that $|\mathscr F(n, k)| = n|\mathscr F(n - 1, k)| - |\mathscr F(n - 2, k)|.$ The explicit formula then follows from a standard generating function argument.
\end{proof}
\end{frame}

\begin{frame}
  \frametitle{Existence of DCS coefficients for some Fibonaccian lattices}
\begin{theorem}[Donnelly-Dunkum]
$\mathscr F(n, 3)$ realizes a representation of the Lie algebra $A_{n + 1}$.
\end{theorem}
Explicit sets of DCS-satisfactory coefficients, which are known to be unique, were calculated by the author for the lattices \(\mathscr F(4, 3)\) and \(\mathscr F(5, 3).\)
\end{frame}

\begin{frame}
  \frametitle{Cardinality of Catalanian lattices}
\begin{theorem}[Malone]
The vertex cardinality of the Catalanian lattice of order \(n, k\) is given by the $k \times k$ Hankel determinant
\begin{align*}
\vert L_c^{Cat}(n, k)\vert = \det M = \begin{vmatrix}
c_n & c_{n+1} & \cdots & c_{n+k-1} \\
c_{n+1} & c_{n+2} & \cdots & c_{n+k} \\
\vdots & \vdots & \ddots & \vdots \\
c_{n+k-1} & c_{n+k} & \cdots & c_{n+2k-2}
\end{vmatrix}
\end{align*}
If we index this matrix by $i, j = 0, 1, \dots, k - 1$, then $m_{ij}$, i.e., the $ij$th entry of $M$, is $c_{n + i + j}.$
\end{theorem}
\end{frame}

\begin{frame}
  \frametitle{Uniqueness of DCS coefficients for some Catalanian lattices}
\begin{theorem}[Dunkum-Donnelly-Malone]
If there exist a set of positive rational DCS-satisfactory coefficients for the lattice $\mathscr C(n, k)$, they are uniquely determined.
\end{theorem}
\pause
\begin{lemma}[Gelfand-Tsetlin 1950]
A color-$k$ component of $\mathscr C(n, k)$, $k = 1, \dots, n - 1$, realizes a representation of the Lie algebra $A_{k + 1}.$ Furthermore, the edge coefficients are positive rationals.
\end{lemma}
\end{frame}

\section{Paths and tableaux}
\begin{frame}
  \frametitle{Topside peakless Motzkin paths}
\begin{definition}
A \textit{topside peakless Motzkin path} of length $n$ is a sequence $\{p_i\}_{i=1}^n$ such that for each $1 \leq i \leq n,$
\begin{enumerate}
\tightlist
\item $p_i \in \{-1, 0, 1\};$
\item $\sum_{k=1}^i p_k \geq 0;$
\item if $p_{i - 1} = 1$, $p_i \not = -1,$
\end{enumerate}
and for which $\sum_{i=1}^n p_i = 0.$
\end{definition}
\end{frame}

\begin{frame}
\begin{figure}
\centering
\subfloat{%
\begin{pspicture}(0,0)(4,2)
\psdot*(0,0)
\psline[linewidth=1pt]{->}(0,0)(1,0) \psdot*(1,0)
\psline[linewidth=1pt]{->}(1,0)(2,0) \psdot*(2,0)
\psline[linewidth=1pt]{->}(2,0)(3,0) \psdot*(3,0)
\psline[linewidth=1pt]{->}(3,0)(4,0) \psdot*(4,0)
\end{pspicture}
}\hspace{0.3\textwidth}%
\subfloat{%
\begin{pspicture}(0,0)(4,2)
\psdot*(0,0)
\psline[linewidth=1pt]{->}(0,0)(1,0) \psdot*(1,0)
\psline[linewidth=1pt]{->}(1,0)(2,1) \psdot*(2,1)
\psline[linewidth=1pt]{->}(2,1)(3,1) \psdot*(3,1)
\psline[linewidth=1pt]{->}(3,1)(4,0) \psdot*(4,0)
\end{pspicture}
}\\
\subfloat{%
\begin{pspicture}(0,0)(4,2)
\psdot*(0,0)
\psline[linewidth=1pt]{->}(0,0)(1,1) \psdot*(1,1)
\psline[linewidth=1pt]{->}(1,1)(2,1) \psdot*(2,1)
\psline[linewidth=1pt]{->}(2,1)(3,0) \psdot*(3,0)
\psline[linewidth=1pt]{->}(3,0)(4,0) \psdot*(4,0)
\end{pspicture}
}\hspace{0.3\textwidth}%
\subfloat{%
\begin{pspicture}(0,0)(4,2)
\psdot*(0,0)
\psline[linewidth=1pt]{->}(0,0)(1,1) \psdot*(1,1)
\psline[linewidth=1pt]{->}(1,1)(2,1) \psdot*(2,1)
\psline[linewidth=1pt]{->}(2,1)(3,1) \psdot*(3,1)
\psline[linewidth=1pt]{->}(3,1)(4,0) \psdot*(4,0)
\end{pspicture}
}
\end{figure}
\end{frame}

\begin{frame}
  \frametitle{Topside peakless Motzkin paths}
\begin{proposition}
Denote the number of topside peakless Motzkin paths of length $n$ by $P_n.$ Then we have the recurrence
\begin{align*}
P_n = \begin{cases} 1,& n = 0 \\ P_{n-1} + \sum_{i=3}^n P_{i-2}P_{n - i},  & n = 1, 2, \dots\end{cases}.
\end{align*}
\end{proposition}
\end{frame}

\begin{frame}
  \frametitle{Littlewood-Richardson tableaux}

\begin{definition}
Let $P, Q \in \mathbb Z^n$ be weakly decreasing $n$-tuples of nonnegative integers with $p_i > q_i$ for each $i \in [n].$ A \textit{Littlewood-Richardson tableau} of shape $P/Q$ is an assignment of $1$, $2$, or $3$ to each empty square in a diagram such as the one on the next slide satisfying the following conditions:
\begin{enumerate}
\tightlist
\item each row is weakly increasing, read left to right;
\item each column is strongly increasing, read top to bottom;
\item when following the diagram by rows, \textit{right to left,} top to bottom, it is always true that the count of 1s is at least the count of 2s is at least the count of 3s.
\end{enumerate}
\end{definition}
\end{frame}

\begin{frame}
  \frametitle{Littlewood-Richardson tableaux}
\begin{figure}
\centering
\begin{ytableau}
\none & \none & \none & & &\\
\none & \none & & \\
\none & \none & \none[\vdots] \\
\none & & \\
&
\end{ytableau}
\caption{An example (unfilled) tableau.}
\end{figure}
(There are $p_i$ total spaces on the $i$th row, $q_i$ of which are unfilled.)
\end{frame}

\begin{frame}
  \frametitle{Littlewood-Richardson tableaux}
We will mainly be concerned with tableaux of the shape
\((n, n-1, n-3, \dots)/(n - 2, n - 4, \dots)\). (The dots indicate that
the shape vector entries decrease to zero rather than becoming
negative.)

\begin{figure}
\centering
\subfloat{%
\begin{ytableau}
\none & \none & 1 & 1 \\
1 & 1 & 2 \\
2
\end{ytableau}
}\hfill%
\subfloat{%
\begin{ytableau}
\none & \none & 1 & 1 \\
1 & 1 & 2 \\
3
\end{ytableau}
}\hfill%
\subfloat{%
\begin{ytableau}
\none & \none & 1 & 1 \\
1 & 2 & 2 \\
2
\end{ytableau}
}\hfill%
\subfloat{%
\begin{ytableau}
\none & \none & 1 & 1 \\
1 & 2 & 2 \\
3
\end{ytableau}
}
\end{figure}

\end{frame}

\begin{frame}
  \frametitle{Equinumeracy conjecture}
\begin{conjecture}
For any $n \geq 0$, the number of LR tableaux of the given shape is given by \(P_n.\)
\end{conjecture}

This is confirmed computationally up to at least \(n = 26\) (at which both sequences number \(560954047.\))
\end{frame}

\section{Code demo}
\begin{frame}
  \begin{center}
    \huge Thank you very much.
\end{center}
\end{frame}
\end{document}
